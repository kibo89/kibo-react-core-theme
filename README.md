# Kibo React Theme

Kibo theme built on top of React JS.

### Available Hypr blocks after extending page.hypr
1. {% block dropzone-area %}{% endblock dropzone-area %}
2. {% block before-react-container %}{% endblock before-react-container %}
3. {% block after-react-container %}{% endblock after-react-container %}
4. {% block title-tag-content %}{% endblock title-tag-content %}

### How To Use Dropzone
1. Add dropzone tag on page specific hypr (ex. home.hypr)
   ```
   {% dropzone zoneId="homeBanner" scope="page" %}
   ```
2. Use Dropzone component. ex.
   1. Without model object
       ```html
       <Dropzone zoneId="homeBanner" />
       ```
   2. With model object
       ```html
       <script>
            const myModelObject = {} ;
       </script>

       <Dropzone zoneId="homeBanner" model={myModelObject} />
       ```
3. You have to use the [lodash template](https://lodash.com/docs/4.17.15#template) syntax in a widget's html/hypr file.

BEWARE - All the HTML from widgets dropped on page gets download on page load. (Try to keep it as small as possible)

## Available Actions

## `Build`

`yarn build` or `yarn build --profile`\
Builds the app for production to the `build` folder. <br />
It correctly bundles React in production mode and optimizes the build for the best performance.\
If you want to profile the production app then add `--profile` flag while running this command.

## `Upload`

`node .kibo-build/build.js -k theme_application_key -u kibo_username -p kibo_password -i kibo_account_id --upload`<br />
This command upload the files form **build** and **kibo-core** directory to Kibo Theme

## `Wipe`

`node .kibo-build/build.js -k theme_application_key -u kibo_username -p kibo_password -i kibo_account_id --wipe`<br />
This command wipe all the files form Kibo Theme

## `Wipe + Upload`

`node .kibo-build/build.js -k theme_application_key -u kibo_username -p kibo_password -i kibo_account_id --wipe --upload`<br />
This command wipe all the files form Kibo Theme and then upload the files form **build** and **kibo-core** directory to
Kibo Theme
