import * as React from 'react';
import _ from 'lodash';
import Api from '../api/Api';
import PageContext from '../@types/pageContext/PageContext';

export interface DropzoneProps<T> {
  zoneId: string;
  hintText?: string;
  model?: T;
}

class Dropzone<T> extends React.Component<DropzoneProps<T>> {
  api = new Api();
  private readonly dropzoneEditable: boolean;
  private hyprDropzoneContent: string;
  private readonly dropZoneConfig: unknown;

  constructor(props: DropzoneProps<T>) {
    super(props);

    const hyprDropzoneEl = document.getElementById(`mz-drop-zone-${this.props.zoneId}`);
    // data-drop-zone attribute is available in edit mode only
    this.dropZoneConfig = hyprDropzoneEl?.getAttribute('data-drop-zone');
    this.dropzoneEditable = !!this.dropZoneConfig;
    this.hyprDropzoneContent = hyprDropzoneEl?.innerHTML || '';
    hyprDropzoneEl?.remove();
  }

  refCallback = (r: HTMLDivElement): void => {
    const isEditMode = (this.api.getPreloadData('pagecontext') as PageContext).isEditMode;

    if (isEditMode && this.dropzoneEditable) {
      document.dispatchEvent(
        new CustomEvent<HTMLDivElement | null>('considerNewDropzone', { detail: r }),
      );
    }
  };

  render(): JSX.Element {
    if (this.dropzoneEditable) {
      // Get existing content only if present
      const dropzoneSelector = document.querySelector('#mz-drop-zone-' + this.props.zoneId);
      const dataWidgetBlocks = dropzoneSelector?.querySelectorAll(':scope div[data-widget]');

      let content = this.hyprDropzoneContent;
      if (dataWidgetBlocks && dataWidgetBlocks.length > 1) {
        // Widget configured
        content = dropzoneSelector?.innerHTML || '';
        this.hyprDropzoneContent = ''; // This will take care of duplicate content on re-renders.
      }

      const dataDropZoneJson = this.dropZoneConfig || '{"id":"' + this.props.zoneId + '","scope":"Page","span":12}';
      return (
        <div
          ref={this.refCallback}
          id={'mz-drop-zone-' + this.props.zoneId}
          className="mz-drop-zone mz-cms-editing mz-cms-grid"
          data-hint-text={this.props.hintText || 'Drop Widget Here'}
          data-drop-zone={dataDropZoneJson}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      );
    } else {
      const lodashTemplate = this.hyprDropzoneContent;
      const compiled = _.template(lodashTemplate)(this.props.model || {});

      return <div ref={this.refCallback} dangerouslySetInnerHTML={{ __html: compiled }} />;
    }
  }
}

export default Dropzone;
