import * as React from 'react';
import Product from '../@types/product/Product';
import Grid from '@material-ui/core/Grid';
import LazyLoad from 'react-lazy-load';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink } from 'react-router-dom';
import BasketPlus from 'mdi-material-ui/BasketPlus';
import HeartPlusOutline from 'mdi-material-ui/HeartPlusOutline';
import ShareIcon from '@material-ui/icons/Share';
import IconButton from '@material-ui/core/IconButton';
import ProductPrice from '../common/ProductPrice';

const maxImageHeight = 150;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    productImage: {
      width: 'auto',
      height: 'auto',
      maxHeight: maxImageHeight + 'px',
      maxWidth: '100%',
    },
    mainImageContainer: {
      backgroundColor: theme.palette.grey[200],
      height: theme.spacing(2) + maxImageHeight + 'px',
      display: 'flex !important',
      alignItems: 'center',
      '& > div': {
        margin: 'auto',
        display: 'flex',
        alignItems: 'center',
      },
    },
    imgNotAvailable: {
      margin: 'auto',
    },
    gridCard: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    actionArea: {
      height: '90%',
    },
  }),
);

export default function ProductGridItem(props: Record<'item', Product>): JSX.Element {
  const classes = useStyles();

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.gridCard}>
        <CardActionArea className={classes.actionArea} component={RouterLink} to={`/p/${props.item.productCode}`}>
          <div className={classes.mainImageContainer}>
            {props.item.content.productImages && props.item.content.productImages.length > 0 ? (
              <LazyLoad height={150}>
                <img
                  src={`${props.item.content.productImages[0].imageUrl}?max=150&quality=75`}
                  alt={props.item.content.productImages[0].altText}
                  className={classes.productImage}
                />
              </LazyLoad>
            ) : (
              <Typography variant="h6" className={classes.imgNotAvailable}>
                Image not available
              </Typography>
            )}
          </div>
          <CardContent>
            <Typography gutterBottom variant="body1">
              <span dangerouslySetInnerHTML={{ __html: props.item.content.productName }} />
            </Typography>

            <ProductPrice productDetails={props.item} />
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Grid item>
              <Button size="small" color="primary" startIcon={<BasketPlus />}>
                Add to Cart
              </Button>
            </Grid>
            <Grid item>
              <IconButton aria-label="add to favorites">
                <HeartPlusOutline />
              </IconButton>
              <IconButton aria-label="share">
                <ShareIcon />
              </IconButton>
            </Grid>
          </Grid>
        </CardActions>
      </Card>
    </Grid>
  );
}
