import * as React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';

import { productSearchRequestExtractor, productSearchResultExtractor, search } from '../appState/ProductSearchReducer';
import IProductSearchResult from '../api/search/IProductSearchResult';
import IProductSearchRequest from '../api/search/IProductSearchRequest';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  }),
);

export default function PageSize(): JSX.Element {
  const classes = useStyles();
  const productSearchResult: IProductSearchResult = useSelector(productSearchResultExtractor);
  const productSearchRequest: IProductSearchRequest = useSelector(productSearchRequestExtractor);
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    productSearchRequest.pageSize = parseInt(`${event.target.value}`);
    dispatch(search(productSearchRequest));
  };

  return (
    <FormControl variant="outlined" size={'small'} className={classes.formControl}>
      <InputLabel id="page-size-box-label">Page Size</InputLabel>
      <Select
        labelId="page-size-box-label"
        id="page-size-box"
        value={productSearchResult.pageSize}
        label="Page Size"
        onChange={handleChange}
      >
        <MenuItem value="" disabled>
          Page Size
        </MenuItem>
        {[...Array(10).keys()].map((arrItem) => (
          <MenuItem key={arrItem} value={(arrItem + 3) * 3}>
            {(arrItem + 3) * 3}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
