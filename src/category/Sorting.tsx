import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useDispatch, useSelector } from 'react-redux';
import { productSearchRequestExtractor, productSearchResultExtractor, search } from '../appState/ProductSearchReducer';
import IProductSearchRequest, { SortBy } from '../api/search/IProductSearchRequest';
import IProductSearchResult from '../api/search/IProductSearchResult';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  }),
);

export default function Sorting(): JSX.Element {
  const classes = useStyles();
  const productSearchResult: IProductSearchResult = useSelector(productSearchResultExtractor);
  const productSearchRequest: IProductSearchRequest = useSelector(productSearchRequestExtractor);
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    switch (event.target.value as string) {
      case 'price asc': {
        productSearchRequest.sortBy = SortBy.ASC_PRICE;
        break;
      }
      case 'price desc': {
        productSearchRequest.sortBy = SortBy.DESC_PRICE;
        break;
      }
      case 'productName asc': {
        productSearchRequest.sortBy = SortBy.ASC_PRODUCT_NAME;
        break;
      }
      case 'productName desc': {
        productSearchRequest.sortBy = SortBy.DESC_PRODUCT_NAME;
        break;
      }
      case 'createDate asc': {
        productSearchRequest.sortBy = SortBy.ASC_CREATE_DATE;
        break;
      }
      case 'createDate desc': {
        productSearchRequest.sortBy = SortBy.DESC_CREATE_DATE;
        break;
      }
    }
    dispatch(search(productSearchRequest));
  };

  return (
    <FormControl variant="outlined" size={'small'} className={classes.formControl}>
      <InputLabel id="product-sort-box-label">Sort By</InputLabel>
      <Select
        labelId="product-sort-box-label"
        id="product-sort-box"
        value={productSearchResult.currentSort || productSearchRequest.sortBy}
        label="Sort By"
        onChange={handleChange}
      >
        <MenuItem value="" disabled>
          Sort By
        </MenuItem>
        <MenuItem value="price asc">Price: Low to High</MenuItem>
        <MenuItem value="price desc">Price: High to Low</MenuItem>
        <MenuItem value="productName asc">Alphabetical: A-Z</MenuItem>
        <MenuItem value="productName desc">Alphabetical: Z-A</MenuItem>
        <MenuItem value="createDate desc">Date Added: Most Recent First</MenuItem>
        <MenuItem value="createDate asc">Date Added: Most Recent Last</MenuItem>
      </Select>
    </FormControl>
  );
}
