import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import _ from 'lodash';

import IProductSearchResult from '../api/search/IProductSearchResult';
import IProductSearchRequest from '../api/search/IProductSearchRequest';
import SearchApi from '../api/search/SearchApi';
import SearchApiObject from '../api/search/SearchApiObject';
import Facet, { FacetValue } from '../@types/Facet';
import {
  productSearchResultExtractor,
  anonymousSearchResultExtractor,
  productSearchRequestExtractor,
  anonymousSearchRequestExtractor,
  searchApiExtractor,
  anonymousSearchApiExtractor,
  search,
  anonymousSearch,
  syncWithAnonymousSearchResult,
  syncWithSearchResult,
} from '../appState/ProductSearchReducer';
import { useState } from 'react';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    facet: {
      width: '100%',
      margin: 0,
      '&:hover, &:focus': {
        backgroundColor: theme.palette.grey[200],
      },
    },
    facetContent: {
      padding: 0,
    },
  }),
);

type FacetCheckboxProps = {
  facet: Facet;
  facetValue: FacetValue;
  detached: boolean;
};

function useForceUpdate() {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [value, setValue] = useState(0);
  return () => setValue((value) => value + 1);
}

function FacetCheckbox(props: FacetCheckboxProps) {
  const searchApi: SearchApi<SearchApiObject> = props.detached
    ? useSelector(anonymousSearchApiExtractor)
    : useSelector(searchApiExtractor);
  const dispatch = useDispatch();
  const forceUpdate = useForceUpdate();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    searchApi.facetSelectionChanged(props.facet.field, props.facetValue.value, event.target.checked);
    forceUpdate();
    props.detached ? dispatch(anonymousSearch()) : dispatch(search());
  };

  return (
    <Checkbox
      name={props.facet.field}
      checked={props.facetValue.isApplied}
      onChange={handleChange}
      disabled={searchApi.isPending()}
    />
  );
}

export default function Facets(props: Record<'detached', boolean>): JSX.Element {
  const classes = useStyles();

  const productSearchResult: IProductSearchResult = props.detached
    ? useSelector(anonymousSearchResultExtractor)
    : useSelector(productSearchResultExtractor);
  const searchApi: SearchApi<SearchApiObject> = props.detached
    ? useSelector(anonymousSearchApiExtractor)
    : useSelector(searchApiExtractor);
  const productSearchRequest: IProductSearchRequest = useSelector(productSearchRequestExtractor);
  const anonymousSearchRequest: IProductSearchRequest = useSelector(anonymousSearchRequestExtractor);
  const dispatch = useDispatch();
  const { categoryId } = useParams<Record<'categoryId', string>>();
  searchApi.setCategoryId(categoryId);

  const clearAll = _.throttle((): void => {
    const newReqConfig = searchApi.getRequestConfig();

    if (newReqConfig.facetValueFilter && newReqConfig.facetValueFilter !== '') {
      newReqConfig.facetValueFilter = undefined;
      props.detached ? dispatch(anonymousSearch(newReqConfig)) : dispatch(search(newReqConfig));
    }
  }, 200);

  const reset = _.throttle((): void => {
    if (!_.isEqual(productSearchRequest, anonymousSearchRequest)) {
      // Apply only if there is change in object so we can avoid extra network call
      dispatch(syncWithSearchResult());
    }
  }, 200);

  const applyFilters = _.throttle((): void => {
    if (!_.isEqual(productSearchRequest, anonymousSearchRequest)) {
      // Apply only if there is change in object so we can avoid extra network call
      dispatch(syncWithAnonymousSearchResult());
    }
  }, 200);

  return (
    <>
      {productSearchResult.facets.map((facet) => {
        if (facet.values && facet.values.length) {
          return (
            <Accordion key={facet.field}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls={`facet-${facet.field}-content`}
                id={`facet-${facet.field}-header`}
              >
                <Typography className={classes.heading}>{facet.label}</Typography>
              </AccordionSummary>
              <AccordionDetails className={classes.facetContent} id={`facet-${facet.field}-content`}>
                <Grid container>
                  {facet.values.map((facetValue) => (
                    <Grid item key={facetValue.filterValue} sm={12}>
                      <FormGroup row>
                        <FormControlLabel
                          className={classes.facet}
                          control={<FacetCheckbox facet={facet} facetValue={facetValue} detached={props.detached} />}
                          label={facetValue.label}
                          disabled={searchApi.isPending()}
                        />
                      </FormGroup>
                    </Grid>
                  ))}
                </Grid>
              </AccordionDetails>
            </Accordion>
          );
        }

        return null;
      })}
      {!props.detached && (
        <Box mt={2} mb={2}>
          <Button
            variant="contained"
            color="primary"
            disabled={searchApi.isPending()}
            onClick={clearAll}
            fullWidth={true}
          >
            Clear All Filters
          </Button>
        </Box>
      )}
      {props.detached && (
        <>
          <Box mt={2} mb={2}>
            <ButtonGroup color="primary" aria-label="Facet filtering actions" fullWidth={true}>
              <Button color="primary" disabled={searchApi.isPending()} onClick={clearAll}>
                Clear All Filters
              </Button>
              <Button color="primary" disabled={searchApi.isPending()} onClick={reset}>
                Reset Filters
              </Button>
            </ButtonGroup>
          </Box>
          <Box mt={2} mb={2}>
            <Button
              variant="contained"
              color="primary"
              disabled={searchApi.isPending()}
              onClick={applyFilters}
              fullWidth={true}
            >
              Apply Filters
            </Button>
          </Box>
        </>
      )}
    </>
  );
}
