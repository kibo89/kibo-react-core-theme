import * as React from 'react';
import ProductGridItem from './ProductGridItem';
import ProductGridItemSkeleton from './ProductGridItemSkeleton';
import IProductSearchResult from '../api/search/IProductSearchResult';
import { useSelector } from 'react-redux';
import { productSearchResultExtractor } from '../appState/ProductSearchReducer';

export default function ProductGrid(): JSX.Element {
  const productSearchResult: IProductSearchResult = useSelector(productSearchResultExtractor);

  return (
    <>
      {productSearchResult
        ? productSearchResult.items.map((item) => <ProductGridItem key={item.productCode} item={item} />)
        : [...Array(12).keys()].map((item) => <ProductGridItemSkeleton key={item} />)}
    </>
  );
}
