import * as React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';

export default function ProductGridItemSkeleton(): JSX.Element {
  return (
    <Grid item xs={4}>
      <Skeleton variant="rect" width="100%" height={150} />
      <Skeleton />
      <Skeleton width="60%" />
    </Grid>
  );
}
