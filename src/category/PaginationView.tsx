import * as React from 'react';
import Pagination from '@material-ui/lab/Pagination';
import IProductSearchResult from '../api/search/IProductSearchResult';
import { useDispatch, useSelector } from 'react-redux';
import { productSearchResultExtractor, search, searchApiExtractor } from '../appState/ProductSearchReducer';
import SearchApi from '../api/search/SearchApi';
import SearchApiObject from '../api/search/SearchApiObject';

export default function PaginationView(): JSX.Element {
  const productSearchResult: IProductSearchResult = useSelector(productSearchResultExtractor);
  const searchApi: SearchApi<SearchApiObject> = useSelector(searchApiExtractor);
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    searchApi.currentPage = value;
    dispatch(search());
  };

  return (
    <Pagination
      color="primary"
      onChange={handleChange}
      count={productSearchResult.pageCount}
      defaultPage={1}
      page={searchApi.currentPage}
      boundaryCount={2}
      showFirstButton
      showLastButton
    />
  );
}
