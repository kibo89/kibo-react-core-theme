import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Dropzone from '../kibo/Dropzone';
import Facets from './Facets';
import Sorting from './Sorting';
import PaginationView from './PaginationView';
import PageSize from './PageSize';
import ProductGrid from './ProductGrid';
import IProductSearchResult from '../api/search/IProductSearchResult';
import Loading from '../common/Loading';
import { useSelector } from 'react-redux';
import { productSearchResultExtractor, searchApiExtractor } from '../appState/ProductSearchReducer';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: 8,
    },
  }),
);

export default function DesktopView(): JSX.Element {
  const classes = useStyles();
  const searchApi = useSelector(searchApiExtractor);
  const productSearchResult: IProductSearchResult = useSelector(productSearchResultExtractor);

  if (searchApi.isPending()) {
    return <Loading />;
  } else if (
    !productSearchResult ||
    (productSearchResult && !productSearchResult.items && !productSearchResult.facets)
  ) {
    return <div>Products are not associated with category</div>;
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Grid container alignContent="center" spacing={2}>
            <Grid item>
              <Dropzone zoneId="categoryBanner" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={4} lg={3}>
              <Facets detached={false} />
            </Grid>
            <Grid item xs={12} sm={12} md={8} lg={9}>
              <Grid container spacing={2} alignItems="center" justify="space-between">
                <Grid item>
                  <Sorting />
                </Grid>
                <Grid item>
                  <PaginationView />
                </Grid>
                <Grid item>
                  <PageSize />
                </Grid>
              </Grid>
              <Grid container spacing={2} alignItems="stretch">
                <ProductGrid />
              </Grid>
              <Grid container spacing={2} alignItems="center" justify="space-between">
                <Grid item>
                  <Sorting />
                </Grid>
                <Grid item>
                  <PaginationView />
                </Grid>
                <Grid item>
                  <PageSize />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
