import * as React from 'react';
import { isMobileOnly } from 'react-device-detect';
import { useParams } from 'react-router-dom';
import DesktopView from './DesktopView';
import MobileView from './MobileView';
import { useDispatch, useSelector } from 'react-redux';
import { searchApiExtractor, search } from '../appState/ProductSearchReducer';
import SearchApi from '../api/search/SearchApi';
import SearchApiObject from '../api/search/SearchApiObject';

// const isTablet = !isMobileOnly && (isAndroid || isIOS);

export default function Category(): JSX.Element {
  const { categoryId } = useParams<Record<'categoryId', string>>();
  const searchApi: SearchApi<SearchApiObject> = useSelector(searchApiExtractor);
  const dispatch = useDispatch();

  if (searchApi.getCategoryId() !== categoryId) {
    searchApi.setCategoryId(categoryId);
    dispatch(search());
  }

  if (isMobileOnly) {
    return <MobileView />;
  }

  // Tablet and desktop view
  return <DesktopView />;
}
