export interface FacetValue {
  childrenFacetValues?: FacetValue[];
  count: number;
  filterValue: string;
  isApplied: boolean;
  isDisplayed: boolean;
  label: string;
  parentFacetValue: string;
  rangeQueryValueEnd: string;
  rangeQueryValueStart: string;
  value: string;
}

interface Facet {
  isFaceted: boolean;
  facetType: string;
  field: string;
  label: string;
  values: FacetValue[];
}

export default Facet;
