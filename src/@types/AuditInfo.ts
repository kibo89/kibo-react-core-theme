type AuditInfo = {
  updateDate: string;
  createDate: string;
  updateBy: string;
  createBy: string;
};

export default AuditInfo;
