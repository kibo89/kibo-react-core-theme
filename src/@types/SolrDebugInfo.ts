interface SolrDebugInfo {
  blockedProductCodes: string;
  boostedProductCodes: string;
  boostFunctions: string;
  boostQueries: string;
  searchTuningRuleCode: string;
}

export default SolrDebugInfo;
