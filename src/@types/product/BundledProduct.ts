import ProductContent from './ProductContent';
import InventoryInfo from './InventoryInfo';
import PackageMeasurements from './PackageMeasurements';

interface BundledProduct {
  content: ProductContent;
  creditValue: number;
  goodsType: string;
  inventoryInfo: InventoryInfo;
  isPackagedStandAlone: boolean;
  measurements: PackageMeasurements;
  optionAttributeFQN: string;
  optionValue: unknown;
  productCode: string;
  productType: string;
  quantity: number;
}

export default BundledProduct;
