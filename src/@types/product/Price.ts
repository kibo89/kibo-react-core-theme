import ProductDiscount from './ProductDiscount';

interface Price {
  catalogListPrice: number;
  catalogSalePrice?: number;
  creditValue?: number;
  discount?: ProductDiscount;
  effectivePricelistCode: string;
  msrp?: number;
  price: number;
  priceListEntryCode: string;
  priceListEntryMode: string;
  priceType: string;
  salePrice?: number;
  salePriceType?: string;
}

export default Price;
