import BundledProduct from './BundledProduct';
import AttributeVocabularyValueDisplayInfo from './AttributeVocabularyValueDisplayInfo';

interface OptionValue {
  attributeValueId: number;
  bundledProduct?: BundledProduct;
  deltaPrice?: number;
  deltaWeight?: number;
  displayInfo?: AttributeVocabularyValueDisplayInfo;
  isDefault?: boolean;
  isEnabled: boolean;
  isSelected: boolean;
  shopperEnteredValue?: unknown;
  stringValue: string;
  value: string | number | boolean;
}

export default OptionValue;
