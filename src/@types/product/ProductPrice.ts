import AppliedDiscount from './AppliedDiscount';

interface ProductPrice {
  catalogListPrice?: number;
  catalogSalePrice?: number;
  creditValue?: number;
  discount?: AppliedDiscount;
  effectivePricelistCode?: string;
  msrp?: number;
  price: number;
  priceListEntryCode?: string;
  priceListEntryMode?: string;
  priceType?: string;
  salePrice?: number;
  salePriceType?: string;
}

export default ProductPrice;
