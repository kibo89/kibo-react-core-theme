interface PricingBehavior {
  discountsRestricted: boolean;
  discountsRestrictedEndDate?: Date;
  discountsRestrictedStartDate?: Date;
}

export default PricingBehavior;
