import ProductImage from './ProductImage';
import MetaTagDetails from '../MetaTagDetails';

interface ProductContent extends MetaTagDetails {
  productFullDescription?: string;
  productImages: ProductImage[];
  productName: string;
  productShortDescription?: string;
  seoFriendlyUrl: string;
}

export default ProductContent;
