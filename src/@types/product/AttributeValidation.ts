interface AttributeValidation {
  maxDateValue: Date;
  maxNumericValue: number;
  maxStringLength: number;
  minDateValue: Date;
  minNumericValue: number;
  minStringLength: number;
  regularExpression: string;
}

export default AttributeValidation;
