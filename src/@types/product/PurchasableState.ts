import PurchasableMessage from './PurchasableMessage';

interface PurchasableState {
  isPurchasable: boolean;
  messages?: PurchasableMessage[];
}

export default PurchasableState;
