import LowerUpperPriceRange from './LowerUpperPriceRange';

interface PriceRange {
  lower: LowerUpperPriceRange;
  upper: LowerUpperPriceRange;
}

export default PriceRange;
