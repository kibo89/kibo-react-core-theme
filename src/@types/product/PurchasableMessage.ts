interface PurchasableMessage {
  message: string;
  severity: string;
  source: string;
  sourceId: string;
  validationType: string;
}

export default PurchasableMessage;
