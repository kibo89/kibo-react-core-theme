import ImageResource from '../ImageResource';

interface ProductImage extends ImageResource {
  productImageGroupId: string;
}

export default ProductImage;
