import AttributeDetail from './AttributeDetail';
import ProductPropertyValue from './ProductPropertyValue';

interface ProductProperty {
  attributeDetail: AttributeDetail;
  attributeFQN: string;
  isHidden: boolean;
  isMultiValue: boolean;
  values: ProductPropertyValue[];
}

export default ProductProperty;
