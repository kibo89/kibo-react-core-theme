import AttributeDetail from './AttributeDetail';
import OptionValue from './OptionValue';

interface Option {
  attributeDetail: AttributeDetail;
  attributeFQN: string;
  isMultiValue: boolean;
  isProductImageGroupSelector: boolean;
  isRequired: boolean;
  values: OptionValue[];
}

export default Option;
