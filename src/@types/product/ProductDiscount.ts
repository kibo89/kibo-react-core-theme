import Discount from '../Discount';

interface ProductDiscount {
  couponCode: string;
  discount: Discount;
  discounts: Discount[];
  impact: number;
}

export default ProductDiscount;
