interface Measurement {
  unit: string;
  value: number;
}

export default Measurement;
