import ProductPrice from './ProductPrice';
import PriceRange from './PriceRange';

interface VolumePriceBand {
  isCurrent: boolean;
  maxQty: number;
  minQty: number;
  price: ProductPrice;
  priceRange: PriceRange;
}

export default VolumePriceBand;
