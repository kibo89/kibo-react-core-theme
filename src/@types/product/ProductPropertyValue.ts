import AttributeVocabularyValueDisplayInfo from './AttributeVocabularyValueDisplayInfo';

interface ProductPropertyValue {
  displayInfo?: AttributeVocabularyValueDisplayInfo;
  stringValue?: string;
  value: unknown;
}

export default ProductPropertyValue;
