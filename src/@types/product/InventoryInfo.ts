interface InventoryInfo {
  manageStock: boolean;
  onlineLocationCode?: string;
  onlineSoftStockAvailable?: number;
  onlineStockAvailable?: number;
  outOfStockBehavior?: string;
}

export default InventoryInfo;
