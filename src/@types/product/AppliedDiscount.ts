import Discount from '../Discount';

interface AppliedDiscount {
  couponCode: string;
  discount: Discount;
  discounts: Discount[];
  impact: number;
}

export default AppliedDiscount;
