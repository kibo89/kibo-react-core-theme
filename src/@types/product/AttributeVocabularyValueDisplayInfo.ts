interface AttributeVocabularyValueDisplayInfo {
  cmsId: string;
  colorValue: string;
  imageUrl: string;
}

export default AttributeVocabularyValueDisplayInfo;
