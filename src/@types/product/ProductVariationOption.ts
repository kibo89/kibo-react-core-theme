interface ProductVariationOption {
  attributeFQN: string;
  value: unknown;
  valueSequence: number;
}

export default ProductVariationOption;
