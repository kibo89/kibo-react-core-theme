import Measurement from './Measurement';

interface PackageMeasurements {
  packageHeight: Measurement;
  packageLength: Measurement;
  packageWeight: Measurement;
  packageWidth: Measurement;
}

export default PackageMeasurements;
