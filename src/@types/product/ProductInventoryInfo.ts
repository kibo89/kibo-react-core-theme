interface ProductInventoryInfo {
  manageStock: boolean;
  onlineLocationCode?: string;
  onlineSoftStockAvailable?: number;
  onlineStockAvailable?: number;
  outOfStockBehavior?: string;
}

export default ProductInventoryInfo;
