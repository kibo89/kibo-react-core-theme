import AttributeValidation from './AttributeValidation';

interface AttributeDetail {
  allowFilteringAndSortingInStorefront: boolean;
  customWeightInStorefrontSearch: boolean;
  dataType: string;
  dataTypeSequence: number;
  description?: string;
  displayIntention?: string;
  indexValueWithCase?: boolean;
  inputType: string;
  name: string;
  searchableInStorefront: boolean;
  searchDisplayValue: boolean;
  usageType: string;
  validation?: AttributeValidation;
  valueType: string;
}

export default AttributeDetail;
