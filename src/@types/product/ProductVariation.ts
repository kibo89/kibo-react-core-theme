import InventoryInfo from './InventoryInfo';
import ProductVariationOption from './ProductVariationOption';

interface ProductVariation {
  inventoryInfo: InventoryInfo;
  options: ProductVariationOption[];
  productCode: string;
}

export default ProductVariation;
