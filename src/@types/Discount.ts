interface Discount {
  discountId: number;
  expirationDate: Date;
  friendlyDescription: string;
  impact: number;
  name: string;
}

export default Discount;
