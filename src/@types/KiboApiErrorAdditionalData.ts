interface KiboApiErrorAdditionalData {
  name: string;
  value: string;
}

export default KiboApiErrorAdditionalData;
