type SupressedSmsTransactions = {
  shipmentItemCanceled: boolean;
  shipmentAssigned: boolean;
  customerAtCurbside: boolean;
  customerIntransit: boolean;
  intransitConfirmation: boolean;
  orderConfirmation: boolean;
  shipmentFulfilled: boolean;
  curbsideReady: boolean;
  storeItemsCanceled: boolean;
};

export default SupressedSmsTransactions;
