import Gateways from './Gateways';
import ExternalPaymentWorkflowDefinitions from './ExternalPaymentWorkflowDefinitions';
import PurchaseOrder from './PurchaseOrder';
import JobSettings from './JobSettings';
import AuditInfo from '../AuditInfo';

type PaymentSettings = {
  gateways: Gateways[];
  externalPaymentWorkflowDefinitions: ExternalPaymentWorkflowDefinitions[];
  payByMail: boolean;
  purchaseOrder: PurchaseOrder;
  jobSettings: JobSettings;
  auditInfo: AuditInfo;
};

export default PaymentSettings;
