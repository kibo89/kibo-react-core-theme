export type PaymentTerm = {
  description: string;
  sequenceNumber: number;
  code: string;
};

type PurchaseOrderSettings = {
  isEnabled: boolean;
  allowSplitPayment: boolean;
  customFields: [];
  paymentTerms: PaymentTerm[];
};

export default PurchaseOrderSettings;
