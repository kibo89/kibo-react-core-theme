export type PaymentTerm = {
  code: string;
  sequenceNumber: number;
  description: string;
};

type PurchaseOrder = {
  isEnabled: boolean;
  paymentTerms: PaymentTerm[];
  allowSplitPayment: boolean;
  customFields: [];
};

export default PurchaseOrder;
