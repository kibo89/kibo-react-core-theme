export type ExternalPaymentWorkflowCredentials = {
  displayName: string;
  apiName: string;
  vocabularyValues: [];
  inputType: string;
  isSensitive: boolean;
};

type ExternalPaymentWorkflowDefinitions = {
  name: string;
  namespace: string;
  fullyQualifiedName: string;
  isEnabled: boolean;
  isLegacy: boolean;
  credentials: ExternalPaymentWorkflowCredentials[];
};

export default ExternalPaymentWorkflowDefinitions;
