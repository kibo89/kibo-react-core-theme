export type GatewaySupportedCard = {
  type: string;
  friendlyName: string;
  paymentType: string;
};

export type CredentialDefinition = {
  name: string;
  displayName: string;
  adminDisplayOrder: number;
  isPublic: boolean;
};

type GatewayDefinition = {
  id: string;
  countryCode: string;
  name: string;
  prodServiceURL: string;
  testServiceURL: string;
  integrationImplTypeName: string;
  supportedCards: GatewaySupportedCard[];
  features: string[];
  credentialDefinitions: CredentialDefinition[];
  administationUi: [];
};

export default GatewayDefinition;
