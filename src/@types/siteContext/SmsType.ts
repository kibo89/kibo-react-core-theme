type SmsType = {
  enabled?: boolean;
  id: string;
};

export default SmsType;
