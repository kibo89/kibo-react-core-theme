import ExternalPaymentTypeCredentials from './ExternalPaymentTypeCredentials';

type ExternalPaymentWorkflowSettings = {
  name: string;
  namespace: string;
  fullyQualifiedName: string;
  isEnabled: boolean;
  credentials: ExternalPaymentTypeCredentials[];
};

export default ExternalPaymentWorkflowSettings;
