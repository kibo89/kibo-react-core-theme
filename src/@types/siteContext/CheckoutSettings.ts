import VisaCheckoutSettings from './VisaCheckoutSettings';
import PurchaseOrderSettings from './PurchaseOrderSettings';
import ExternalPaymentWorkflowSettings from './ExternalPaymentWorkflowSettings';
import PaymentSettings from './PaymentSettings';

type CheckoutSettings = {
  customerCheckoutType: string;
  paymentProcessingFlowType: string;
  payByMail: boolean;
  useOverridePriceToCalculateDiscounts: boolean;
  isPayPalEnabled: boolean;
  purchaseOrder: PurchaseOrderSettings;
  supportedCards: unknown;
  visaCheckout: VisaCheckoutSettings;
  externalPaymentWorkflowSettings: ExternalPaymentWorkflowSettings[];
  supportedGiftCards: unknown;
  paymentSettings: PaymentSettings;
};

export default CheckoutSettings;
