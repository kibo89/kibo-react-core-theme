import GeneralSettings from './GeneralSettings';
import CheckoutSettings from './CheckoutSettings';
import DomainSettings from './DomainSettings';
import CurrencyInfo from './CurrencyInfo';

export type Domains = {
  current: DomainSettings;
  primary: DomainSettings;
  all: DomainSettings[];
};

type SiteContext = {
  siteExists: boolean;
  tenantId: number;
  siteId: number;
  hashString: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  labels: Record<string, any>;
  themeId: string;
  generalSettings: GeneralSettings;
  checkoutSettings: CheckoutSettings;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  themeSettings: Record<string, any>;
  isEditMode: boolean;
  cdnPrefix: string;
  secureHost: string;
  supportsInStorePickup: boolean;
  domains: Domains;
  currencyInfo: CurrencyInfo;
};

export default SiteContext;
