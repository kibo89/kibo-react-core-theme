import SupressedEmailTransactions from './SupressedEmailTransactions';
import EmailType from './EmailType';
import SmsType from './SmsType';
import SupressedSmsTransactions from './SupressedSmsTransactions';

type GeneralSettings = {
  websiteName: string;
  siteTimeZone: string;
  siteTimeFormat: string;
  adjustForDaylightSavingTime: boolean;
  allowAllIPs: boolean;
  senderEmailAddress: string;
  cdnCacheBustKey: string;
  replyToEmailAddress: string;
  supressedEmailTransactions: SupressedEmailTransactions;
  desktopTheme: Record<string, string>;
  tabletTheme: Record<string, string>;
  isGoogleAnalyticsEnabled: boolean;
  isGoogleAnalyticsEcommerceEnabled: boolean;
  isWishlistCreationEnabled: boolean;
  isMultishipEnabled: boolean;
  allowInvalidAddresses: boolean;
  isAddressValidationEnabled: boolean;
  isRequiredLoginForLiveEnabled: boolean;
  isRequiredLoginForStagingEnabled: boolean;
  customCdnHostName: string;
  emailTypes: EmailType[];
  enforceSitewideSSL: boolean;
  smsTypes: SmsType[];
  supressedSmsTransactions: SupressedSmsTransactions;
};

export default GeneralSettings;
