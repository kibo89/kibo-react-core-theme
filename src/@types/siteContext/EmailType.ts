type EmailType = {
  enabled?: boolean;
  id: string;
  senderEmailAddressOverride?: string;
  senderEmailAliasOverride?: string;
  replyToEmailAddressOverride?: string;
  bccEmailAddressOverride?: string;
  onlyOnApiRequest?: string;
};

export default EmailType;
