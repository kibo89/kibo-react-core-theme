type DomainSettings = {
  domainName: string;
  isPrimary: boolean;
  isSystemAssigned: boolean;
  isDomainManaged: boolean;
  siteId: number;
};

export default DomainSettings;
