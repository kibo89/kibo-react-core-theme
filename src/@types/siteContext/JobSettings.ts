export type AutoCaptureJob = {
  flexibleCapture: unknown;
  isEnabled: boolean;
  interval: number;
};

export type ForceCaptureJob = {
  captureAfterDays: number;
  isEnabled: boolean;
  interval: number;
};

type JobSettings = {
  autoCaptureJob: AutoCaptureJob;
  forceCaptureJob: ForceCaptureJob;
};

export default JobSettings;
