import GatewayDefinition from './GatewayDefinition';

export type GatewayAccount = {
  id: string;
  name: string;
  gatewayDefinitionId: string;
  countryCode: string;
  isActive: boolean;
  credentialFields: [];
  binPatterns: [];
};

type Gateways = {
  gatewayDefinition: GatewayDefinition;
  areGatewayCredentialFieldsSet: boolean;
  gatewayAccount: GatewayAccount;
  supportedCards: [];
  siteGatewaySupportedCards: [];
};

export default Gateways;
