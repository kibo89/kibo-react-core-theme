type VisaCheckoutSettings = {
  isEnabled: boolean;
};

export default VisaCheckoutSettings;
