import KiboApiErrorAdditionalData from './KiboApiErrorAdditionalData';

interface KiboApiError {
  additionalErrorData: KiboApiErrorAdditionalData[];
  applicationName: string;
  category: string;
  errorCode: string;
  items: [];
  message: string;
}

export default KiboApiError;
