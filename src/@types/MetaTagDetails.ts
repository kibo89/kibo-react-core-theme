interface MetaTagDetails {
  metaTagDescription: string;
  metaTagKeywords: string;
  metaTagTitle: string;
}

export default MetaTagDetails;
