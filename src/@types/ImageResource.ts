interface ImageResource {
  altText?: string;
  cmsId: string;
  imageLabel?: string;
  imageUrl?: string;
  mediaType?: string;
  sequence: number;
  videoUrl?: string;
}

export default ImageResource;
