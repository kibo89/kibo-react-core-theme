import Navigation from './Navigation';

interface StoreWindow extends Window {
  navigation: Navigation;
}

export default StoreWindow;
