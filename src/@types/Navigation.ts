export type NavigationNode = {
  url: string;
  name: string;
  categoryCode: string;
  categoryId: string;
  nodeType: string;
  isHomePage: boolean;
  expanded: boolean;
  expandable: boolean;
  isLeaf: boolean;
  isEmpty: boolean;
  fqUrl: string;
  items?: NavigationNode[];
};

export type Breadcrumb = {
  url: string;
  name: string;
  nodeType: string;
};

type Navigation = {
  currentNode: NavigationNode;
  breadcrumbs: Breadcrumb[];
  tree: NavigationNode[];
};

export default Navigation;
