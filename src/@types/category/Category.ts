import CategoryContent from './CategoryContent';

interface Category {
  url?: string;
  categoryCode: string;
  categoryId: number;
  childrenCategories: Category[];
  content: CategoryContent;
  count: number;
  isDisplayed: boolean;
  parentCategory: Category;
  parentCategoryId?: number;
  sequence: number;
}

export default Category;
