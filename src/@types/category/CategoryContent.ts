import ImageResource from '../ImageResource';
import MetaTagDetails from '../MetaTagDetails';

interface CategoryContent extends MetaTagDetails {
  categoryImages: ImageResource[];
  description: string;
  name: string;
  pageTitle: string;
  slug: string;
}

export default CategoryContent;
