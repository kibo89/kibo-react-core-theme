export type CmsContextPage = {
  listFQN: string;
  path: string;
  documentTypeFQN: string;
  includeInactiveDocument: boolean;
};

export type CmsContextTemplate = {
  listFQN: string;
  path: string;
};

export type CmsContextSite = {
  listFQN: string;
  path: string;
};

type CmsContext = {
  page: CmsContextPage;
  template: CmsContextTemplate;
  site: CmsContextSite;
};

export default CmsContext;
