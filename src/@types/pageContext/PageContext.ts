import CmsContext from './CmsContext';
import Search from './Search';
import CurrencyInfo from './CurrencyInfo';
import NumberFormat from './NumberFormat';
import PartialPageContext from './PartialPageContext';

interface PageContext extends PartialPageContext {
  themeId: string;
  debugFlags: number;
  sorting: unknown;
  cdnCacheBustKey: string;
  pagination: string;
  isSecure: boolean;
  pageType: string;
  cmsContext: CmsContext;
  search: Search;
  title: string;
  metaDescription: string;
  metaTitle: string;
  metaKeywords: string;
  feedUrl: string;
  url: string;
  dataViewMode: number;
  secureHost: string;
  categoryCode: string;
  categoryId: number;
  currencyInfo: CurrencyInfo;
  numberFormat: NumberFormat;
}

export default PageContext;
