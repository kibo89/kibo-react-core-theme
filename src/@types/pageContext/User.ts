type User = {
  isAuthenticated: boolean;
  userId: string;
  firstName: string;
  lastName: string;
  email?: string;
  isAnonymous: boolean;
  accountId?: number;
  behaviors: number[];
};

export type UserProfile = {
  userId: string;
  firstName: string;
  lastName: string;
  emailAddress?: string;
  userName: string;
};

export type UserVisitDetails = {
  visitId: string;
  visitorId: string;
  isTracked: boolean;
  isUserTracked: boolean;
};

export default User;
