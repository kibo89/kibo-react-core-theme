type Search = {
  categoryId: number;
  facets: null | unknown;
};

export default Search;
