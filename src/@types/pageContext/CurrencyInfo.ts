interface CurrencyInfo {
  currencyCode: number;
  englishName: string;
  symbol: string;
  precision: string;
  roundingType: number;
}

export default CurrencyInfo;
