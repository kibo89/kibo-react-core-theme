import User, { UserProfile, UserVisitDetails } from './User';

export type CrawlerInfo = { isCrawler: boolean; canonicalUrl?: string };

export default interface PartialPageContext {
  correlationId: string;
  ipAddress: string;
  isDebugMode: boolean;
  isCrawler: boolean;
  isMobile: boolean;
  isTablet: boolean;
  isDesktop: boolean;
  visit: UserVisitDetails;
  user: User;
  userProfile: UserProfile;
  isEditMode: boolean;
  isAdminMode: boolean;
  now: string;
  crawlerInfo: CrawlerInfo;
  currencyRateInfo: unknown;
}
