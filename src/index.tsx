import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Bugsnag from '@bugsnag/js';
import BugsnagPluginReact from '@bugsnag/plugin-react';
import MomentUtils from '@date-io/moment';
import App from './App';
import reportWebVitals from './reportWebVitals';
import reduxStore from './appState/ReduxStore';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

Bugsnag.start({
  apiKey: 'c0feed331fdb4c44681e3b505f57f858',
  plugins: [new BugsnagPluginReact()],
});

const ErrorBoundary = Bugsnag.getPlugin('react')?.createErrorBoundary(React);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={reduxStore}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        {ErrorBoundary && (
          <ErrorBoundary>
            <App />
          </ErrorBoundary>
        )}
        {!ErrorBoundary && <App />}
      </MuiPickersUtilsProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
