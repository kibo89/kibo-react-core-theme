import * as React from 'react';
import { useEffect, useState } from 'react';
import Slider, { Settings } from 'react-slick';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import clsx from 'clsx';
import _ from 'lodash';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import Product from '../@types/product/Product';
import ProductImage from '../@types/product/ProductImage';
import Loading from '../common/Loading';
import Typography from '@material-ui/core/Typography';
import '../scss/slick-arrows-pdp.scss';
import ProductApi from '../api/product/ProductApi';
import { useSelector } from 'react-redux';
import { currentProductApiExtractor } from '../appState/ProductReducer';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    productImage: {
      width: 'auto',
      maxHeight: '500px',
      height: 'auto',
      maxWidth: '100%',
      marginLeft: 'auto',
      marginRight: 'auto',
      display: 'block',
    },
    hidden: {
      display: 'none',
    },
    mainImageContainer: {
      padding: theme.spacing(2),
      backgroundColor: theme.palette.grey[200],
      height: '500px',
      display: 'flex !important',
      alignItems: 'center',
    },
    mainImgFailedToLoad: {
      height: '500px',
    },
    sliderImageBox: {
      display: 'flex !important',
      alignItems: 'center',
      outline: 'none',
      width: '90% !important',
      height: '90% !important',
      cursor: 'pointer',
      border: '1px solid grey',
      padding: '.5rem',
      margin: '.2rem',
    },
    activeSliderImageBox: {
      border: '1px solid black',
      backgroundColor: 'gainsboro',
    },
    imgLoadFailure: {
      margin: 'auto',
    },
  }),
);

export default function ProductImages(): JSX.Element {
  const [currentImage, setCurrentImage] = useState<ProductImage>();
  const [mainImageLoaded, setMainImageLoaded] = useState<boolean>(false);
  const [failedToLoad, setFailedToLoad] = useState<boolean>(false);
  const [productDetails, setProductDetails] = useState<Product>();
  const productApi: ProductApi | undefined = useSelector(currentProductApiExtractor);
  const classes = useStyles();

  const setMainImage = _.throttle((productImage: ProductImage) => {
    if (currentImage?.cmsId !== productImage.cmsId) {
      setFailedToLoad(false);
      setMainImageLoaded(false);
      setCurrentImage(productImage);
    }
  }, 100);

  useEffect(() => {
    // Why - If anything goes wrong at any point of time then that should not affect the image view; and product
    // details view should handle and display the error on screen rather than wiping out whatever shown on screen.
    if (productApi && !productApi.isApiError(productApi.getInstance().data)) {
      if (!productDetails || !_.isEqual(productApi.getInstance().data, productDetails)) {
        const productDetails = productApi.getData() as Product;
        setProductDetails(productDetails);

        if (productDetails.content.productImages && productDetails.content.productImages.length) {
          setMainImage(productDetails.content.productImages[0]);
        }
      }
    }

    if (
      !currentImage &&
      productDetails &&
      productDetails.content.productImages &&
      productDetails.content.productImages.length
    ) {
      setCurrentImage(productDetails.content.productImages[0]);
    }
  });

  const sliderProps: Settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    lazyLoad: 'ondemand',
    swipeToSlide: true,
    arrows: true,
    prevArrow: <ChevronLeftIcon />,
    nextArrow: <ChevronRightIcon />,
  };

  return (
    <div>
      <div className={classes.mainImageContainer}>
        {!productDetails && (
          <Typography variant="h6" className={classes.imgLoadFailure}>
            Failed to load an image
          </Typography>
        )}
        {productDetails && productDetails.content.productImages && productDetails.content.productImages.length > 0 ? (
          <>
            {currentImage && (
              <img
                src={`${currentImage.imageUrl}?max=500&quality=75`}
                alt={currentImage.altText}
                className={clsx(classes.productImage, mainImageLoaded ? '' : classes.hidden)}
                onLoad={() => {
                  setMainImageLoaded(true);
                  setFailedToLoad(false);
                }}
                onError={() => setFailedToLoad(true)}
              />
            )}
            {!failedToLoad && !mainImageLoaded && <Loading blockClass={classes.mainImgFailedToLoad} iconSize={100} />}
            {failedToLoad && (
              <Typography variant="h6" className={classes.imgLoadFailure}>
                Failed to load an image
              </Typography>
            )}
          </>
        ) : (
          <Typography variant="h6" className={classes.imgLoadFailure}>
            Images not available
          </Typography>
        )}
      </div>
      {productDetails && productDetails.content.productImages && productDetails.content.productImages.length > 0 && (
        <Slider {...sliderProps}>
          {productDetails.content.productImages.map((prodImage) => {
            return (
              <div
                key={prodImage.cmsId}
                className={clsx(
                  classes.sliderImageBox,
                  prodImage.cmsId === currentImage?.cmsId ? classes.activeSliderImageBox : '',
                )}
                onClick={() => setMainImage(prodImage)}
              >
                <img src={`${prodImage.imageUrl}?max=50&quality=75`} alt={prodImage.altText} />
              </div>
            );
          })}
        </Slider>
      )}
    </div>
  );
}
