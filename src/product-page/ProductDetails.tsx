import * as React from 'react';
import { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import { CartPlus, CartVariant, HeartPlus, Star, StarHalfFull, StarOutline } from 'mdi-material-ui';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';

import ProductOption from './ProductOption';
import Product from '../@types/product/Product';
import ProductPrice from '../common/ProductPrice';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import ProductApi from '../api/product/ProductApi';
import { useSelector } from 'react-redux';
import { currentProductApiExtractor } from '../appState/ProductReducer';
import _ from 'lodash';
import Bugsnag from '@bugsnag/js';
import KiboApiError from '../@types/KiboApiError';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    reviewRatingLink: {
      paddingLeft: theme.spacing(2),
    },
    addToCartButton: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
    },
    actionButtonsArea: {
      margin: theme.spacing(2),
      padding: theme.spacing(4),
    },
  }),
);

export default function ProductDetails(): JSX.Element {
  const classes = useStyles();
  const [productDetails, setProductDetails] = useState<Product>();
  const productApi: ProductApi | undefined = useSelector(currentProductApiExtractor);

  useEffect(() => {
    // Why - If anything goes wrong at any point of time then that should not affect the image view and product
    // details view should handle and display the error on screen rather than wiping out whatever shown on screen.
    if (productApi && !productApi.isApiError(productApi.getInstance().data)) {
      if (!productDetails || !_.isEqual(productApi.getInstance().data, productDetails)) {
        setProductDetails(productApi.getInstance().data as Product);
      }
    }
  });

  if (!productDetails || !productApi) {
    return (
      <Box bgcolor="error.main" p={1}>
        Product details not yet loaded or failed to load
      </Box>
    );
  }

  return (
    <div>
      {productApi.isApiError(productApi.getInstance().data) && (
        <Box bgcolor="error.main" p={2}>
          {(productApi.getInstance().data as KiboApiError).message}
        </Box>
      )}
      <Typography variant="h5" component="h1">
        <span dangerouslySetInnerHTML={{ __html: productDetails.content.productName }} /> (#
        {productDetails.productCode})
      </Typography>

      <Box>
        <Box component="span" color="warning.main">
          <Star />
          <Star />
          <Star />
          <StarHalfFull />
          <StarOutline />
        </Box>
        <Link className={classes.reviewRatingLink} href="#rating-and-review" variant="body2">
          5,212 ratings
        </Link>
        <Divider />
      </Box>

      <ProductPrice productDetails={productDetails} />

      {(productDetails.mfgPartNumber || productDetails.upc) && (
        <Box pt={2} pb={2}>
          {productDetails.mfgPartNumber && <div>Mfg. Part Number: {productDetails.mfgPartNumber}</div>}
          {productDetails.upc && <div>UPC: {productDetails.upc}</div>}
        </Box>
      )}

      {productDetails.options && (
        <Box pt={2} pb={2}>
          <Grid container spacing={3}>
            {productDetails.options.map((option) => (
              <Grid key={option.attributeFQN} item sm={12} md={6}>
                <ProductOption option={option} />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}

      <Paper variant="outlined" className={classes.actionButtonsArea}>
        <Grid container justify="center" alignItems="center" spacing={2}>
          <Grid item xs={12} sm={6}>
            <Button variant="outlined" color="primary" startIcon={<HeartPlus />} fullWidth>
              Add to Wishlist
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button variant="outlined" color="primary" startIcon={<CartPlus />} fullWidth>
              Add to Cart
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Box pt={2}>
              <Button
                className={classes.addToCartButton}
                startIcon={<CartVariant />}
                fullWidth
                onClick={() => Bugsnag.notify(new Error('Test error'))}
              >
                Buy Now
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Paper>

      {productDetails.content.productShortDescription && (
        <Box>
          <Typography variant="h6" component="h2">
            Product Details
          </Typography>
          <Divider />
          <Typography dangerouslySetInnerHTML={{ __html: productDetails.content.productShortDescription }} />
        </Box>
      )}
    </div>
  );
}
