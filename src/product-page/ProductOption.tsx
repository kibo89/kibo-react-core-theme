import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import FormHelperText from '@material-ui/core/FormHelperText';
import { KeyboardDatePicker } from '@material-ui/pickers';
import OutlinedInput from '@material-ui/core/OutlinedInput';

import Option from '../@types/product/Option';
import Amount from '../common/Amount';
import OptionValue from '../@types/product/OptionValue';
import moment from 'moment/moment';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

export default function ProductOption(props: Record<'option', Option>): JSX.Element {
  const classes = useStyles();
  const option = props.option;
  const selectedValue = option.values.find((optionValue) => optionValue.isSelected);

  const getDeltaPrice = (optionValue?: OptionValue) => {
    return optionValue && optionValue.deltaPrice && optionValue.deltaPrice > 0 ? (
      <>
        (<Amount amount={optionValue.deltaPrice} /> more)
      </>
    ) : (
      <></>
    );
  };

  if (option.attributeDetail.inputType === 'List') {
    let value = selectedValue ? selectedValue.value : '';
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
      value = event.target.value as string;
    };

    return (
      <FormControl className={classes.formControl} variant="outlined" size="small" fullWidth>
        <InputLabel htmlFor={option.attributeFQN + '-field'}>{option.attributeDetail.name}</InputLabel>
        <Select
          label={option.attributeDetail.name}
          inputProps={{ id: option.attributeFQN + '-field' }}
          id={option.attributeFQN + '-select-box'}
          value={value}
          multiple={option.isMultiValue}
          onChange={handleChange}
        >
          {option.values &&
            option.values.map((optionValue) => (
              <MenuItem key={optionValue.value as string} value={optionValue.value as string}>
                {optionValue.value}
                {getDeltaPrice(optionValue)}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
    );
  } else if (option.attributeDetail.inputType === 'YesNo') {
    const [checked, setChecked] = React.useState(selectedValue?.isSelected || false);
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setChecked(event.target.checked);
    };

    return (
      <FormControl className={classes.formControl} size="small">
        <FormControlLabel
          className={classes.formControl}
          control={<Switch checked={checked} onChange={handleChange} name={option.attributeFQN} color="primary" />}
          label={option.attributeDetail.name}
          aria-describedby={option.attributeFQN + '-helper-text'}
        />
        <FormHelperText id={option.attributeFQN + '-helper-text'}>
          {getDeltaPrice(option.values && option.values.length > 0 ? option.values[0] : undefined)}
        </FormHelperText>
      </FormControl>
    );
  } else if (option.attributeDetail.inputType === 'TextBox') {
    return (
      <FormControl className={classes.formControl} fullWidth variant="outlined" size="small">
        <InputLabel htmlFor={option.attributeFQN}>{option.attributeDetail.name}</InputLabel>
        <OutlinedInput
          id={option.attributeFQN}
          label={option.attributeDetail.name}
          value={option.values[0].value}
          aria-describedby={option.attributeFQN + '-helper-text'}
          type={option.attributeDetail.dataType === 'Number' ? 'number' : 'text'}
        />
        <FormHelperText id={option.attributeFQN + '-helper-text'}>
          {getDeltaPrice(option.values && option.values.length > 0 ? option.values[0] : undefined)}
        </FormHelperText>
      </FormControl>
    );
  } else if (option.attributeDetail.inputType === 'TextArea') {
    return (
      <FormControl className={classes.formControl} fullWidth variant="outlined" size="small">
        <InputLabel htmlFor={option.attributeFQN}>{option.attributeDetail.name}</InputLabel>
        <OutlinedInput
          id={option.attributeFQN}
          label={option.attributeDetail.name}
          value={option.values[0].value}
          aria-describedby={option.attributeFQN + '-helper-text'}
          multiline
          rows={4}
        />
        <FormHelperText id={option.attributeFQN + '-helper-text'}>
          {getDeltaPrice(option.values && option.values.length > 0 ? option.values[0] : undefined)}
        </FormHelperText>
      </FormControl>
    );
  } else if (option.attributeDetail.inputType === 'Date') {
    const [selectedDate, setSelectedDate] = React.useState<Date | null>(new Date());
    const handleDateChange = (date: Date | null) => {
      setSelectedDate(date);
    };

    return (
      <KeyboardDatePicker
        size="small"
        fullWidth
        inputVariant="outlined"
        disableToolbar
        variant="inline"
        format="MM/DD/YY"
        margin="normal"
        id={option.attributeFQN}
        label={option.attributeDetail.name}
        value={selectedDate}
        onChange={(date) => {
          handleDateChange(moment(date).toDate());
        }}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
    );
  }

  return <></>;
}
