import * as React from 'react';
import { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ProductImages from './ProductImages';
import ProductDetails from './ProductDetails';
import ProductApi from '../api/product/ProductApi';
import { useParams } from 'react-router-dom';
import Loading from '../common/Loading';
import { useDispatch, useSelector } from 'react-redux';
import { currentProductApiExtractor, sync } from '../appState/ProductReducer';
import Box from '@material-ui/core/Box';
import { AppDispatch } from '../appState/ReduxStore';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function ProductView(): JSX.Element {
  const classes = useStyles();
  const { productCode } = useParams<Record<'productCode', string>>();
  const productApi: ProductApi | undefined = useSelector(currentProductApiExtractor);
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    if (productCode && (!productApi || productApi.getProductCode() !== productCode)) {
      dispatch(sync(productCode, true));
    }
  });

  if (!productApi) return <Loading />;

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        {productApi.isNotFound() && (
          <Grid item xs={12}>
            <Box bgcolor="error.main" p={2}>
              The product your are trying to view is not found.
            </Box>
          </Grid>
        )}
        <Grid item sm={12} md={5}>
          <ProductImages />
        </Grid>
        <Grid item sm={12} md={7}>
          <ProductDetails />
        </Grid>
        <Grid item xs={12}></Grid>
        <Grid item xs={12} id="rating-and-review"></Grid>
      </Grid>
    </div>
  );
}
