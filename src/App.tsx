import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import loadable from '@loadable/component';

import Header from './header/Header';
import Loading from './common/Loading';

import './scss/theme-fonts.scss';

const Home = loadable(() => import('./home/Home'), {
  fallback: <Loading />,
});
const Category = loadable(() => import('./category/Category'), {
  fallback: <Loading />,
});
const ProductView = loadable(() => import('./product-page/ProductView'), {
  fallback: <Loading />,
});

type RouteContentProps = {
  children: React.ReactNode;
  renderHeader: boolean;
};

function RouteContent(props: RouteContentProps): JSX.Element {
  if (props.renderHeader) {
    return (
      <>
        <Header />

        <main>{props.children}</main>
      </>
    );
  } else {
    return <main>{props.children}</main>;
  }
}

function App(): JSX.Element {
  return (
    <div className="App">
      <CssBaseline />

      <Router>
        <Switch>
          <Route path="/home">
            <RouteContent renderHeader={true}>
              <Home />
            </RouteContent>
          </Route>
          <Route path="/c/:categoryId">
            <RouteContent renderHeader={true}>
              <Category />
            </RouteContent>
          </Route>
          <Route path="/:categoryId/c">
            <RouteContent renderHeader={true}>
              <Category />
            </RouteContent>
          </Route>
          <Route path="/:productCode/p/:productSlug">
            <RouteContent renderHeader={true}>
              <ProductView />
            </RouteContent>
          </Route>
          <Route path="/p/:productCode">
            <RouteContent renderHeader={true}>
              <ProductView />
            </RouteContent>
          </Route>
          <Route path="/">
            <RouteContent renderHeader={true}>
              <Home />
            </RouteContent>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
