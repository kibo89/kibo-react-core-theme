import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(() =>
  createStyles({
    loadingIconSize: {
      fontSize: 5,
    },
  }),
);

export interface LoadingComponentProps {
  blockClass?: string;
  iconSize?: number;
}

export default function Loading(props: LoadingComponentProps): JSX.Element {
  const classes = useStyles();

  return (
    <Grid container direction="row" justify="center" alignItems="center" className={props.blockClass}>
      <Grid item>
        <Box p={5}>
          <CircularProgress className={classes.loadingIconSize} size={props.iconSize} />
        </Box>
      </Grid>
    </Grid>
  );
}
