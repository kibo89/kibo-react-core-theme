import * as React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import Price from '../@types/product/Price';
import Amount from './Amount';

const useStyles = makeStyles(() =>
  createStyles({
    strikedThroughText: {
      textDecoration: 'line-through',
    },
    discountsBlock: {
      color: 'rgb(19, 125, 23)',
    },
  }),
);

export default function PriceView(props: Record<'price', Price>): JSX.Element {
  const classes = useStyles();
  const price = props.price;
  const onSale = !!(price.salePrice || price.salePrice === 0 || price.catalogSalePrice || price.catalogSalePrice === 0);
  const salePrice = price.salePrice || price.catalogSalePrice || 0;
  const discountAvailable = price.discount && price.discount.discounts && price.discount.discounts.length;
  const showMsrp = false;

  return (
    <>
      {onSale && (
        <>
          <Box component="span" fontWeight="fontWeightBold" fontSize="h6.fontSize" color="error.main">
            <Amount amount={salePrice} />
          </Box>
          &nbsp;
          <Box component="span" className={classes.strikedThroughText} color="text.secondary">
            <Amount
              amount={price.catalogListPrice || price.catalogListPrice === 0 ? price.catalogListPrice : price.price}
            />
          </Box>
          {discountAvailable && (
            <Box className={classes.discountsBlock}>
              {price.discount?.discounts.map((discount) => (
                <div key={discount.discountId}>
                  {discount.name} &ndash; <Amount amount={discount.impact} />
                </div>
              ))}
            </Box>
          )}
        </>
      )}
      {!onSale && (
        <Box fontWeight="fontWeightBold" fontSize="h6.fontSize" color="error.main">
          <Amount amount={price.price} />
        </Box>
      )}
      {showMsrp && price.msrp && (
        <Box color="text.secondary">
          MSRP: <Amount amount={price.msrp} />
        </Box>
      )}
    </>
  );
}
