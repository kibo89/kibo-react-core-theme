import * as React from 'react';
import Box from '@material-ui/core/Box';
import Product from '../@types/product/Product';
import PriceView from './Price';

export default function ProductPrice(props: Record<'productDetails', Product>): JSX.Element {
  const product = props.productDetails;

  return (
    <Box pt={1} pb={1}>
      {product.hasPriceRange && product.priceRange && (
        <div aria-label="Price Range">
          <PriceView price={product.priceRange.lower} />
          &ndash;
          <PriceView price={product.priceRange.upper} />
        </div>
      )}
      {!product.hasPriceRange && product.price && <PriceView price={product.price} />}
      {!product.hasPriceRange && !product.price && <span>N/A</span>}
    </Box>
  );
}
