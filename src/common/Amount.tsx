import * as React from 'react';
import Api from '../api/Api';
import PageContext from '../@types/pageContext/PageContext';

export default function Amount(props: Record<'amount', number>): JSX.Element {
  const api = new Api();
  const symbolPosition = 'after';

  // Just like page-context and api-context, site-context will always be available.
  const pageContext = api.getPreloadData<PageContext>('pagecontext') as PageContext;

  if (symbolPosition === 'after') {
    return (
      <>
        {props.amount}
        {pageContext.currencyInfo.symbol}
      </>
    );
  } else {
    return (
      <>
        {pageContext.currencyInfo.symbol}
        {props.amount}
      </>
    );
  }
}
