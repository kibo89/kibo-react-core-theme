import { configureStore } from '@reduxjs/toolkit';
import productSearchReducer from './ProductSearchReducer';
import productReducer from './ProductReducer';

const store = configureStore({
  reducer: {
    productSearch: productSearchReducer,
    product: productReducer,
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
