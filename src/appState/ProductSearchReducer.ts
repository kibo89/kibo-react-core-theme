import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import IProductSearchResult from '../api/search/IProductSearchResult';
import IProductSearchRequest from '../api/search/IProductSearchRequest';
import { AppDispatch } from './ReduxStore';
import SearchApi from '../api/search/SearchApi';
import SearchApiObject from '../api/search/SearchApiObject';

const searchApi = new SearchApi();
const anonymousSearchApi = new SearchApi();

export const productSearchSlice = createSlice({
  name: 'productSearch',
  initialState: {
    request: searchApi.getRequestConfig(),
    result: searchApi.getSearchResult(),
    api: searchApi,
    anonymousSearchRequest: anonymousSearchApi.getRequestConfig(),
    anonymousSearchResult: anonymousSearchApi.getSearchResult(),
    anonymousApi: anonymousSearchApi,
  },
  reducers: {
    setSearchRequest: (state, action: PayloadAction<IProductSearchRequest>) => {
      state.request = action.payload as IProductSearchRequest;
    },
    setAnonymousSearchRequest: (state, action: PayloadAction<IProductSearchRequest>) => {
      state.anonymousSearchRequest = action.payload as IProductSearchRequest;
    },
    syncWithAnonymousSearchResult: (state) => {
      state.result = state.anonymousSearchResult;
    },
    syncWithSearchResult: (state) => {
      state.anonymousSearchResult = state.result;
    },
    setSearchResult: (state, action: PayloadAction<IProductSearchResult>) => {
      state.result = action.payload as IProductSearchResult;
    },
    setAnonymousSearchResult: (state, action: PayloadAction<IProductSearchResult>) => {
      state.anonymousSearchResult = action.payload as IProductSearchResult;
    },
  },
});

export const {
  setSearchRequest,
  setAnonymousSearchRequest,
  syncWithAnonymousSearchResult,
  syncWithSearchResult,
} = productSearchSlice.actions;
const { setSearchResult, setAnonymousSearchResult } = productSearchSlice.actions;

// Thunk
export const search = (request?: IProductSearchRequest) => async (dispatch: AppDispatch): Promise<void> => {
  if (request) searchApi.setRequestConfig(request);

  try {
    await searchApi.getSearchResultPromise();

    dispatch(setSearchResult(searchApi.getSearchResult() as IProductSearchResult));
    dispatch(setSearchRequest(searchApi.getRequestConfig()));
  } catch (err) {
    dispatch(setSearchRequest(searchApi.getRequestConfig()));
  }
};

// Thunk
export const anonymousSearch = (request?: IProductSearchRequest) => async (dispatch: AppDispatch): Promise<void> => {
  if (request) anonymousSearchApi.setRequestConfig(request);
  try {
    await anonymousSearchApi.getSearchResultPromise();

    dispatch(setAnonymousSearchResult(anonymousSearchApi.getSearchResult() as IProductSearchResult));
    dispatch(setAnonymousSearchRequest(anonymousSearchApi.getRequestConfig()));
  } catch (err) {
    dispatch(setAnonymousSearchRequest(anonymousSearchApi.getRequestConfig()));
  }
};

export const productSearchRequestExtractor = (state: {
  productSearch: { request: IProductSearchRequest };
}): IProductSearchRequest => state.productSearch.request;

export const productSearchResultExtractor = (state: {
  productSearch: { result: IProductSearchResult };
}): IProductSearchResult => state.productSearch.result;

export const searchApiExtractor = (state: {
  productSearch: { api: SearchApi<SearchApiObject> };
}): SearchApi<SearchApiObject> => state.productSearch.api;

export const anonymousSearchRequestExtractor = (state: {
  productSearch: { anonymousSearchRequest: IProductSearchRequest };
}): IProductSearchRequest => state.productSearch.anonymousSearchRequest;
export const anonymousSearchResultExtractor = (state: {
  productSearch: { anonymousSearchResult: IProductSearchResult };
}): IProductSearchResult => state.productSearch.anonymousSearchResult;
export const anonymousSearchApiExtractor = (state: {
  productSearch: { anonymousApi: SearchApi<SearchApiObject> };
}): SearchApi<SearchApiObject> => state.productSearch.anonymousApi;

export default productSearchSlice.reducer;
