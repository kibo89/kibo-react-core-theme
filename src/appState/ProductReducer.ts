import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import ProductApi from '../api/product/ProductApi';
import { AppDispatch, RootState } from './ReduxStore';
import KiboApiError from '../@types/KiboApiError';
import Product from '../@types/product/Product';

interface IProductCache {
  productCode: string;
  api: ProductApi;
}

interface IInitialState {
  currentProduct: ProductApi | undefined;
  productCache: IProductCache[];
}

const initialState: IInitialState = {
  currentProduct: undefined,
  productCache: [],
};

const useCacheSizeLimit = false;
const cacheSize = 10;

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setCurrentProduct: (state, action: PayloadAction<string>) => {
      if (action.payload) {
        state.currentProduct = new ProductApi(action.payload);
      }
    },
    setCurrentProductApi: (state, action: PayloadAction<ProductApi>) => {
      if (action.payload) {
        state.currentProduct = action.payload;
      }
    },
    addInCache: (state, action: PayloadAction<ProductApi>) => {
      if (action.payload && !action.payload.isApiError(action.payload.getInstance().data)) {
        if (useCacheSizeLimit && state.productCache.length >= cacheSize) {
          state.productCache.shift();
        }

        state.productCache.push({ productCode: action.payload.getProductCode(), api: action.payload });
      }
    },
    removeProductFromCache: (state, action: PayloadAction<string>) => {
      state.productCache = state.productCache.filter((cacheItem) => cacheItem.productCode !== action.payload);
    },
    clearCache: (state) => {
      state.productCache = [];
    },
  },
});

// (Not to be exposed)
const { setCurrentProductApi, addInCache } = productSlice.actions;

// Thunk
export const sync = (productCode: string, isCurrentProduct?: boolean) => async (
  dispatch: AppDispatch,
  getState: () => RootState,
): Promise<void> => {
  const productCodes: string[] = getState().product.productCache.map((product) => product.productCode);

  // Check if product is already there in cache, if it is then set it as a current product.
  if (productCodes.includes(productCode)) {
    if (isCurrentProduct) {
      const currentProduct = getState().product.productCache.find((product) => product.productCode === productCode)
        ?.api;

      // Casting is fine here as we are pretty sure that we are going to get the api object.
      dispatch(setCurrentProductApi(currentProduct as ProductApi));
    }

    return;
  }

  const productApi = new ProductApi(productCode);
  await productApi.get();

  if (isCurrentProduct) {
    dispatch(setCurrentProductApi(productApi));
  } else {
    dispatch(addInCache(productApi));
  }
};

export const currentProductApiExtractor = (state: RootState): ProductApi | undefined => state.product.currentProduct;

export const currentProductDetailsExtractor = (state: RootState): Product | KiboApiError | undefined =>
  state.product.currentProduct?.getInstance().data;

export const productApiExtractor = (productCode: string) => (state: RootState): ProductApi | undefined =>
  state.product.productCache.find((product) => product.productCode === productCode)?.api;

export const productDetailsExtractor = (productCode: string) => (
  state: RootState,
): Product | KiboApiError | undefined =>
  state.product.productCache.find((product) => product.productCode === productCode)?.api.getInstance()?.data;

export default productSlice.reducer;
