import IGetProductRequestParams from './IGetProductRequestParams';

class ProductRequestParams implements IGetProductRequestParams {
  productCode: string;

  constructor(produceCode: string) {
    this.productCode = produceCode;
  }
}

export default ProductRequestParams;
