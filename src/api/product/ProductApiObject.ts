import ApiObject from '../kibo-storefront-sdk/ApiObject';
import IGetProductRequestParams from './IGetProductRequestParams';
import Product from '../../@types/product/Product';
import KiboApiError from '../../@types/KiboApiError';

interface ProductApiObject extends ApiObject<Product> {
  get(request: IGetProductRequestParams): Promise<ProductApiObject | KiboApiError>;
}

export default ProductApiObject;
