interface IGetProductRequestParams {
  productCode: string;
  variationProductCode?: string;
  allowInactive?: boolean;
  skipInventoryCheck?: boolean;
  supressOutOfStock404?: boolean;
  quantity?: number;
  acceptVariantProductCode?: boolean;
  purchaseLocation?: string;
  variationProductCodeFilter?: string;
  responseFields?: string;
}

export default IGetProductRequestParams;
