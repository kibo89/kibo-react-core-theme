import Api from '../Api';
import ProductApiObject from './ProductApiObject';
import IGetProductRequestParams from './IGetProductRequestParams';
import ProductRequestParams from './ProductRequestParams';
import Product from '../../@types/product/Product';
import KiboApiError from '../../@types/KiboApiError';

class ProductApi extends Api<Product> {
  private readonly request: IGetProductRequestParams;

  constructor(productCode: string) {
    super('product');

    this.request = new ProductRequestParams(productCode);
  }

  getProductCode(): string {
    return this.request.productCode;
  }

  get(): Promise<ProductApiObject | KiboApiError> {
    return this.getInstance<ProductApiObject>().get(this.request);
  }
}

export default ProductApi;
