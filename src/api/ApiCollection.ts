import Api from './Api';
import ApiCollectionType from './kibo-storefront-sdk/ApiCollection';
import ApiObject from './kibo-storefront-sdk/ApiObject';
import IProductSearchRequest from './search/IProductSearchRequest';

class ApiCollection<T> extends Api<T> {
  add(newItems: ApiObject<T> | ApiObject<T>[] | unknown): void {
    (this.getInstance() as ApiCollectionType<T>).add(newItems);
  }

  remove(indexOrItem: number | ApiObject<T>): void {
    (this.getInstance() as ApiCollectionType<T>).remove(indexOrItem);
  }

  replace(newItems: ApiObject<T> | ApiObject<T>[]): void {
    (this.getInstance() as ApiCollectionType<T>).replace(newItems);
  }

  removeAll(): void {
    (this.getInstance() as ApiCollectionType<T>).removeAll();
  }

  getIndex(newIndex: number): number {
    return (this.getInstance() as ApiCollectionType<T>).getIndex(newIndex);
  }

  setIndex(newIndex: number, req: IProductSearchRequest): Promise<ApiCollectionType<T>> {
    return (this.getInstance() as ApiCollectionType<T>).setIndex(newIndex, req);
  }

  firstPage(req: IProductSearchRequest): Promise<ApiCollectionType<T>> {
    return (this.getInstance() as ApiCollectionType<T>).firstPage(req);
  }

  prevPage(req: IProductSearchRequest): Promise<ApiCollectionType<T>> {
    return (this.getInstance() as ApiCollectionType<T>).prevPage(req);
  }

  nextPage(req: IProductSearchRequest): Promise<ApiCollectionType<T>> {
    return (this.getInstance() as ApiCollectionType<T>).nextPage(req);
  }

  lastPage(req: IProductSearchRequest): Promise<ApiCollectionType<T>> {
    return (this.getInstance() as ApiCollectionType<T>).lastPage(req);
  }
}

export default ApiCollection;
