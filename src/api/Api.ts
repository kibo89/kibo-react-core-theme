import PartialPageContext from '../@types/pageContext/PartialPageContext';
import ApiContext from './kibo-storefront-sdk/ApiContext';
import ApiInterface from './kibo-storefront-sdk/ApiInterface';
import ApiInterfaceType from './kibo-storefront-sdk/ApiInterfaceType';
import ApiObject, { ErrorHandler, SuccessHandler } from './kibo-storefront-sdk/ApiObject';
import { BaseApiEventNames } from './kibo-storefront-sdk/ApiEventNames';
import KiboApiError from '../@types/KiboApiError';

interface BrowserWin extends Window {
  MozuSDK: ApiContext;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  parent: any;
}

declare let window: BrowserWin;

interface ApiContextPreload {
  headers: Record<string, string>;
  urls: Record<string, string>;
}

export default class Api<T> {
  private readonly apiObject: ApiObject<T>;
  protected readonly kiboType: ApiInterfaceType;
  public api: ApiInterface;

  constructor(kiboType?: ApiInterfaceType) {
    // Api Context will always be available.
    const apiConfig = this.getPreloadData<ApiContextPreload>('apicontext') as ApiContextPreload;
    window.MozuSDK.setServiceUrls(apiConfig.urls);
    const api = window.MozuSDK.Store(apiConfig.headers).api();
    this.kiboType = kiboType || 'customer';

    this.apiObject = api.createSync(this.kiboType, this.getPreloadData(this.kiboType) || undefined);
    this.api = this.apiObject.api;
  }

  /**
   * Return an instance of Kibo Storefront SDK API interface
   */
  getInstance<R extends ApiObject<T>>(): R {
    return this.apiObject as R;
  }

  /**
   * Returns the API response returned by Kibo in its original form.
   */
  getData(): T | KiboApiError | undefined {
    return this.apiObject.data;
  }

  /**
   * This function will return true only if Kibo returns error saying
   * item is not found for the current site.
   */
  isNotFound(): boolean {
    const data = this.getData();

    return !!data && this.isApiError(data) && (data as KiboApiError).errorCode === 'ITEM_NOT_FOUND';
  }

  /**
   * This function returns true if product is fetched successfully.
   * If Kibo returned an error then this will return false value.
   */
  isAvailable(): boolean {
    const data = this.getData();

    return !!data && !this.isApiError(data);
  }

  /**
   * Returns true if Kibo returned an error
   *
   * @param response Kibo API response
   */
  isApiError(response: T | KiboApiError | undefined): response is KiboApiError {
    return !!response && (response as KiboApiError).errorCode !== undefined;
  }

  /**
   * Get available actions based on the kibo type passed to the constructor, or returns available actions for customer
   * API instance
   */
  getAvailableActions(): string[] {
    return this.apiObject.getAvailableActions();
  }

  /**
   * Get specific property from the response returned by Kibo
   *
   * @param name Name of the property
   * @param value Value of the property
   */
  prop<V>(name: string, value?: V): V | void {
    return this.apiObject.prop(name, value);
  }

  /**
   * Attach Kibo API event to monitor.
   *
   * @param eventName Kibo API event name
   * @param handler Handler function to call when API triggers an event.
   */
  on(eventName: BaseApiEventNames, handler: SuccessHandler<T> | ErrorHandler): void {
    this.apiObject.on(eventName, handler);
  }

  /**
   * Detach Kibo API event registered previously.
   *
   * @param eventName Kibo API event name
   * @param handler Handler function registered previously using on method.
   */
  off(eventName: BaseApiEventNames, handler: SuccessHandler<T> | ErrorHandler): void {
    this.apiObject.off(eventName, handler);
  }

  /**
   * Load preload JSON and return as a provided type.
   * If JSON is not preloaded by Hypr then null value will be returned.
   *
   * @param name Name of the preload data.
   */
  getPreloadData<T>(name: string): T | null {
    const script: HTMLElement | null = document.getElementById('data-mz-preload-' + name);
    let text;
    let obj: T | null = null;

    if (script) {
      text = script.textContent || script.innerText || script.innerHTML;

      if (text) {
        obj = JSON.parse(text);
      }
    }

    if (obj && name == 'pagecontext') {
      const pc = Api.getPartialPageContext();
      obj = Object.assign(obj, pc || {});

      try {
        // Why - Kibo is expected to return consistently isEditMode=true in _mzc cookie which is not happening.
        const pgCtxOnBody = document.getElementsByTagName('body')[0].getAttribute('data-pg-ctx');
        const bodyPgCtx = JSON.parse(`${pgCtxOnBody?.split("'").join('"')}`);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (obj as any).isEditMode = bodyPgCtx.isEditMode.toLocaleLowerCase() === 'true';
      } catch (err) {
        // Try to find if its edit-mode or not by checking Taco object availability in iframe parent and iframe domain.
        if (window.parent?.Taco && window.parent?.location.host.endsWith('mozu.com')) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (obj as any).isEditMode = true;
        }
      }
    }

    if (obj && name == 'user') {
      const pc = Api.getPartialPageContext();
      obj = Object.assign(obj, pc?.user || {});
    }

    return obj;
  }

  /**
   * This method is kept private as its just returning partial page context.
   *
   * @private
   */
  private static getPartialPageContext(): PartialPageContext | null {
    // Similar to Api Context, page context will always be available.
    const pcCookie = document.cookie
      ?.split('; ')
      .map((_: string) => {
        return _.split('=');
      })
      .filter((_) => {
        return _[0] == '_mzPc';
      });

    if (pcCookie && pcCookie.length) {
      try {
        return JSON.parse(window.atob(decodeURIComponent(pcCookie[0][1]))) as PartialPageContext;
      } catch (e) {
        console.log(e);
      }
    }

    return null;
  }
}
