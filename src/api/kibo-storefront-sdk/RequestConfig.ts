import HTTPRequestMethod from './HTTPRequestMethod';

interface RequestConfig {
  url: string;
  verb: HTTPRequestMethod;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  overridePostData: any;
  noBody: boolean;
  iframeTransportUrl: string;
}

export default RequestConfig;
