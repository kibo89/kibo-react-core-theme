interface Context {
  Tenant: number;
  MasterCatalog: number;
  Site: number;
  Catalog: number;
}

export default Context;
