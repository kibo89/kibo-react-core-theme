import RequestConfig from './RequestConfig';
import HTTPRequestMethod from './HTTPRequestMethod';
import ApiInterfaceType from './ApiInterfaceType';
import ApiObject from './ApiObject';
import KiboApiEventNames from './ApiEventNames';
import Context from './Context';

interface ApiInterface {
  context: Context;

  request<T>(method: HTTPRequestMethod, requestConfig: string | RequestConfig, data?: T): Promise<ApiObject<T>>;

  // ApiObject internally calls this method for all the available actions listed for the ApiInterfaceType
  action<T>(
    type: ApiObject<T> | ApiInterfaceType,
    actionName: string,
    data?: T,
    runningOptions?: Record<'silent', boolean>,
  ): Promise<ApiObject<T>>;

  get<T>(type: ApiInterfaceType, data?: T | string): Promise<ApiObject<T>>;

  create<T>(type: ApiInterfaceType, data: T): Promise<ApiObject<T>>;

  update<T>(type: ApiInterfaceType, data: T): Promise<ApiObject<T>>;

  del<T>(type: ApiInterfaceType, data?: T): Promise<ApiObject<T>>;

  createSync<T>(type: ApiInterfaceType, data?: T, runningOptions?: Record<'silent', boolean>): ApiObject<T>;

  getAvailableActionsFor<T>(type: ApiInterfaceType, data?: T): string[];

  on(eventName: KiboApiEventNames, handler: (err: Error, xhr: XMLHttpRequest) => void): void;

  off(eventName: KiboApiEventNames, handler: (err: Error, xhr: XMLHttpRequest) => void): void;

  all<ApiObject>(promises: Promise<unknown>[]): Promise<ApiObject>[];

  steps<ApiObject>(promises: Promise<unknown>[]): Promise<ApiObject>[];

  defer<ApiObject>(): Promise<ApiObject>;
}

export default ApiInterface;
