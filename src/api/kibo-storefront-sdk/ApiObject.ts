import { BaseApiEventNames } from './ApiEventNames';
import ApiInterface from './ApiInterface';
import ApiInterfaceType from './ApiInterfaceType';
import KiboApiError from '../../@types/KiboApiError';

export type ErrorHandler = (err: KiboApiError, xhr: XMLHttpRequest) => void;
export type SuccessHandler<T> = (response: T) => void;

interface ApiObject<T> {
  data: T | KiboApiError | undefined;
  api: ApiInterface;
  type: ApiInterfaceType;

  getAvailableActions(): string[];

  prop<V>(name: string, value?: V): V | void;

  on(eventName: BaseApiEventNames, handler: ErrorHandler | SuccessHandler<T>): void;

  off(eventName: BaseApiEventNames, handler: ErrorHandler | SuccessHandler<T>): void;

  fire(eventName: BaseApiEventNames, data: ApiObject<T> | KiboApiError): void;
}

export default ApiObject;
