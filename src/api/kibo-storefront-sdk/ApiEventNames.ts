export enum BaseApiEventNames {
  ERROR = 'error',
  ACTION = 'action',
  SPAWN = 'spawn',
  SYNC = 'sync',
  SYNC_COMPLETED = 'syncCompleted',
}

export enum ApiEventNames {
  REQUEST = 'request',
  SUCCESS = 'success',
}

type KiboApiEventNames = BaseApiEventNames | ApiEventNames;

export default KiboApiEventNames;
