import ApiObject from './ApiObject';
import IProductSearchRequest from '../search/IProductSearchRequest';

interface ApiCollection<T> extends ApiObject<T> {
  add(newItems: ApiObject<T> | ApiObject<T>[] | unknown): void;

  remove(indexOrItem: number | ApiObject<T>): void;

  replace(newItems: ApiObject<T> | ApiObject<T>[]): void;

  removeAll(): void;

  getIndex(newIndex: number): number;

  setIndex(newIndex: number, req: IProductSearchRequest): Promise<ApiCollection<T>>;

  firstPage(req: IProductSearchRequest): Promise<ApiCollection<T>>;

  prevPage(req: IProductSearchRequest): Promise<ApiCollection<T>>;

  nextPage(req: IProductSearchRequest): Promise<ApiCollection<T>>;

  lastPage(req: IProductSearchRequest): Promise<ApiCollection<T>>;
}

export default ApiCollection;
