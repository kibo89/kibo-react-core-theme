import ApiInterface from './ApiInterface';

interface ApiContext {
  currency: string;
  locale: string;

  Store(contextValues: Record<string, string | number>): ApiContext;
  setServiceUrls(serviceUrls: Record<string, string>): void;
  getServiceUrls(): Record<string, string>;
  api(): ApiInterface;
  asObject(prefix?: string): Record<string, string | number>;
  asHeaders(): Record<string, string | number>;
}

export default ApiContext;
