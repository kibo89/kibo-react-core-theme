import ApiObject from '../kibo-storefront-sdk/ApiObject';
import CustomerDetails from './CustomerDetails';

interface CustomerApiObject extends ApiObject<CustomerDetails> {
  get(accountId: string): Promise<ApiObject<CustomerDetails>>;
}

export default CustomerApiObject;
