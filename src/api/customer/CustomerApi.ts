import Api from '../Api';
import ApiObject from '../kibo-storefront-sdk/ApiObject';
import CustomerApiObject from './CustomerApiObject';
import CustomerDetails from './CustomerDetails';

export default class CustomerApi<T extends CustomerApiObject> extends Api<CustomerDetails> {
  constructor() {
    super('customer');
  }

  get(accountId: string): Promise<ApiObject<CustomerDetails>> {
    return this.getInstance<T>().get(accountId);
  }

  // create(customerDetailsToUpdate: unknown): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().create(this.kiboType, customerDetailsToUpdate);
  // }
  //
  // createStorefront(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().createStorefront();
  // }
  //
  // getPurchaseOrderTransactions(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getPurchaseOrderTransactions();
  // }
  //
  // login(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().login();
  // }
  //
  // loginStorefront(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().loginStorefront();
  // }
  //
  // orderStatusLogin(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().orderStatusLogin();
  // }
  //
  // orderCurbsideEvent(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().orderCurbsideEvent();
  // }
  //
  // orderCurbsideSurveyEvent(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().orderCurbsideSurveyEvent();
  // }
  //
  // update(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().update();
  // }
  //
  // resetPassword(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().resetPassword();
  // }
  //
  // resetPasswordStorefront(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().resetPasswordStorefront();
  // }
  //
  // changePassword(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().changePassword();
  // }
  //
  // getAttributes(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getAttributes();
  // }
  //
  // getAttribute(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getAttribute();
  // }
  //
  // updateAttribute(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().updateAttribute();
  // }
  //
  // getAttributeDefinition(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getAttributeDefinition();
  // }
  //
  // getAttributeDefinitions(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getAttributeDefinitions();
  // }
  //
  // getOrders(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getOrders();
  // }
  //
  // getCards(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getCards();
  // }
  //
  // addCard(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().addCard();
  // }
  //
  // updateCard(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().updateCard();
  // }
  //
  // deleteCard(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().deleteCard();
  // }
  //
  // addContact(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().addContact();
  // }
  //
  // updateContact(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().updateContact();
  // }
  //
  // getContacts(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getContacts();
  // }
  //
  // deleteContact(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().deleteContact();
  // }
  //
  // getCredits(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getCredits();
  // }
  //
  // getCredit(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getCredit();
  // }
  //
  // updateCustomerContacts(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().updateCustomerContacts();
  // }
  //
  // postconstruct(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().postconstruct();
  // }
  //
  // savePaymentCard(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().savePaymentCard();
  // }
  //
  // deletePaymentCard(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().deletePaymentCard();
  // }
  //
  // getStoreCredits(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getStoreCredits();
  // }
  //
  // getDigitalCredit(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getDigitalCredit();
  // }
  //
  // addStoreCredit(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().addStoreCredit();
  // }
  //
  // getReturnLabel(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getReturnLabel();
  // }
  //
  // getFulfillmentReturnLabel(): Promise<ApiObject<CustomerDetails>> {
  //   return this.getInstance<T>().getFulfillmentReturnLabel();
  // }
}
