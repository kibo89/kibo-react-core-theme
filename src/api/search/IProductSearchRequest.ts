export enum SortBy {
  ASC_PRICE = 'price asc',
  DESC_PRICE = 'price desc',
  ASC_PRODUCT_NAME = 'productName asc',
  DESC_PRODUCT_NAME = 'productName desc',
  ASC_CREATE_DATE = 'createDate asc',
  DESC_CREATE_DATE = 'createDate desc',
}

interface IProductSearchRequest {
  sortBy: SortBy;
  pageSize: number;
  startIndex: number;
  query: string;
  filter?: string;
  facetTemplate?: string;
  facetTemplateSubset?: string;
  facet?: string;
  facetFieldRangeQuery?: string;
  facetHierPrefix?: string;
  facetHierValue?: string;
  facetHierDepth?: string;
  facetStartIndex?: string;
  facetPageSize?: string;
  facetSettings?: string;
  facetValueFilter?: string;
  searchSettings?: string;
  enableSearchTuningRules?: boolean;
  searchTuningRuleContext?: string;
  searchTuningRuleCode?: string;
  facetTemplateExclude?: string;
  facetPrefix?: string;
  responseOptions?: string;
  cursorMark?: string;
  facetValueSort?: string;
  sortDefinitionName?: string;
  defaultSortDefinitionName?: string;
  responseFields?: string;

  [requestParam: string]: string | boolean | number | SortBy | undefined;
}

export default IProductSearchRequest;
