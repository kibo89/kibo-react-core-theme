import ApiObject from '../kibo-storefront-sdk/ApiObject';
import IProductSearchResult from './IProductSearchResult';
import ApiCollection from '../kibo-storefront-sdk/ApiCollection';
import IProductSearchRequest from './IProductSearchRequest';

interface SearchApiObject extends ApiObject<IProductSearchResult> {
  get(request: IProductSearchRequest): Promise<ApiCollection<IProductSearchResult>>;
}

export default SearchApiObject;
