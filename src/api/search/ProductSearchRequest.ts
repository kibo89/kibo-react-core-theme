import IProductSearchRequest, { SortBy } from './IProductSearchRequest';

class ProductSearchRequest implements IProductSearchRequest {
  sortBy = SortBy.ASC_CREATE_DATE;
  pageSize = 15;
  startIndex = 0;
  query = '*:*';

  [requestParam: string]: string | boolean | number | SortBy;
}

export default ProductSearchRequest;
