import Product from '../../@types/product/Product';
import Facet from '../../@types/Facet';
import SolrDebugInfo from '../../@types/SolrDebugInfo';

interface IProductSearchResult {
  facets: Facet[];
  items: Product[];
  pageCount: number;
  pageSize: number;
  searchEngine: string;
  startIndex: number;
  totalCount: number;

  currentSort?: string;
  lastIndex?: number;
  currentPage?: number;
  firstIndex?: number;
  middlePageNumbers?: number[];
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;
  nextCursorMark?: string;
  solrDebugInfo?: SolrDebugInfo;
}

export default IProductSearchResult;
