import _ from 'lodash';
import ApiCollection from '../kibo-storefront-sdk/ApiCollection';
import { BaseApiEventNames } from '../kibo-storefront-sdk/ApiEventNames';
import Api from '../Api';
import SearchApiObject from './SearchApiObject';

import IProductSearchRequest, { SortBy } from './IProductSearchRequest';
import IProductSearchResult from './IProductSearchResult';
import ProductSearchRequest from './ProductSearchRequest';
import Facet from '../../@types/Facet';
import KiboApiError from '../../@types/KiboApiError';

class SearchApi<T extends SearchApiObject> extends Api<IProductSearchResult> {
  private productSearchRequest: IProductSearchRequest;
  private productSearchResult: IProductSearchResult | undefined;
  private syncRequests: Promise<ApiCollection<IProductSearchResult>>[] = [];
  private readonly hierarchyDepth = 2;
  private pending = false;
  private categoryId?: string;
  private searchQuery?: string;

  constructor(productSearchRequest?: IProductSearchRequest, productSearchResult?: IProductSearchResult) {
    super('search');

    if (productSearchResult) {
      this.productSearchResult = _.cloneDeep(productSearchResult);
    }

    if (productSearchRequest) {
      this.productSearchRequest = _.cloneDeep(productSearchRequest);
    } else {
      this.productSearchRequest = new ProductSearchRequest();

      this.buildRequestFromUrl(this.productSearchRequest);
    }

    this.productSearchRequest.facetHierDepth = 'categoryId:' + this.hierarchyDepth;
  }

  getCategoryId(): string | undefined {
    return this.categoryId;
  }

  setCategoryId(categoryId: string): void {
    this.categoryId = categoryId;

    this.setHierarchy('categoryId', this.categoryId);
    this.productSearchRequest.query = '*:*';
  }

  getSearchQuery(): string | undefined {
    return this.searchQuery;
  }

  setSearchQuery(searchQuery: string): void {
    // Search page
    const urlSearchParams = new URL(window.location.href).searchParams;
    this.searchQuery = searchQuery || urlSearchParams.get('query') || '*:*';
    this.productSearchRequest.query = this.searchQuery;
    this.productSearchRequest.facet = 'categoryId';
  }

  getRequestConfig(): IProductSearchRequest {
    return _.cloneDeep(this.productSearchRequest);
  }

  setRequestConfig(productSearchRequest: IProductSearchRequest): void {
    this.productSearchRequest = _.cloneDeep(productSearchRequest);
  }

  getSearchResult(): IProductSearchResult | undefined {
    return this.productSearchResult;
  }

  getSearchResultPromise(): Promise<IProductSearchResult | KiboApiError> {
    return new Promise<IProductSearchResult | KiboApiError>((resolve, reject) => {
      const handler = () => {
        this.off(BaseApiEventNames.SYNC_COMPLETED, handler);

        resolve(this.getSearchResult() as IProductSearchResult);
      };
      this.on(BaseApiEventNames.SYNC_COMPLETED, handler);

      const errorHandler = (err: KiboApiError) => {
        this.off(BaseApiEventNames.ERROR, errorHandler);

        reject(err);
      };
      this.on(BaseApiEventNames.ERROR, errorHandler);

      this.sync();
    });
  }

  isPending(): boolean {
    return this.pending;
  }

  public buildRequestFromUrl(productSearchRequest: IProductSearchRequest): void {
    const queryParams = new URLSearchParams(window.location.search);

    type ProductSearchRequestKey = keyof IProductSearchRequest;
    Object.keys(productSearchRequest).forEach((requestParam) => {
      if (queryParams.has(requestParam)) {
        productSearchRequest[requestParam] = queryParams.get(requestParam) as ProductSearchRequestKey;
      }
    });
  }

  private buildUrlFromRequest(): string {
    return Object.keys(this.productSearchRequest).reduce((previousValue, currentValue) => {
      if (previousValue && previousValue.trim().length && this.productSearchRequest[currentValue]) {
        return `${previousValue}&${currentValue}=${this.productSearchRequest[currentValue]}`;
      } else if (this.productSearchRequest[currentValue]) {
        return `${currentValue}=${this.productSearchRequest[currentValue]}`;
      } else if (previousValue && previousValue.trim().length) {
        return previousValue;
      }

      return '';
    });
  }

  /**
   * Pull and return the data based on current state of the request object.
   */
  public sync = _.throttle((): void => {
    this.pending = true;
    this.syncRequests.push(this.getInstance<T>().get(this.productSearchRequest));

    Promise.all(this.syncRequests).then((resp) => {
      this.pending = false;
      this.syncRequests = [];

      // Pick the result of last request only.
      this.productSearchResult = resp[resp.length - 1].data as IProductSearchResult;

      this.getInstance().fire(BaseApiEventNames.SYNC_COMPLETED, resp[resp.length - 1]);

      return Promise.resolve(this.productSearchResult);
    });
  }, 100);

  /**
   * Return an index (starting with 1) of the first product on a page.
   * First product index listed on page (n) will be (pageSize * n - 1) + 1
   * @private
   */
  private firstIndex(): number {
    return this.productSearchRequest.startIndex + 1;
  }

  /**
   * Return an index (starting with 1) of the last product on a page.
   * Last product index listed on page (n) will be firstIndex of 1st product on page +
   * items available on current page
   * @private
   */
  private lastIndex(): number {
    return (
      this.productSearchRequest.startIndex + (this.productSearchResult ? this.productSearchResult.items.length : 0)
    );
  }

  getFacetedUrl(): string {
    const parsedUrl = new URL(window.location.href);
    parsedUrl.search = `?${this.buildUrlFromRequest()}`;

    return parsedUrl.href;
  }

  get searchTerm(): string {
    return this.productSearchRequest.query;
  }

  set searchTerm(searchTerm: string) {
    this.productSearchRequest.query = searchTerm;
  }

  get sort(): SortBy {
    return this.productSearchRequest.sortBy;
  }

  set sort(sortBy: SortBy) {
    this.productSearchRequest.sortBy = sortBy;
  }

  get currentPage(): number {
    return Math.ceil(this.firstIndex() / (this.productSearchRequest.pageSize || 1));
  }

  set currentPage(pageNumber: number) {
    if (pageNumber > 0 && (!this.productSearchResult || pageNumber <= this.productSearchResult.pageCount)) {
      this.productSearchRequest.startIndex =
        pageNumber * this.productSearchRequest.pageSize - this.productSearchRequest.pageSize;
    }
  }

  hasPreviousPage(): boolean {
    return this.productSearchRequest.startIndex > 0;
  }

  hasNextPage(): boolean {
    return this.lastIndex() < (this.productSearchResult?.totalCount || 0);
  }

  set pageSize(pageSize: number) {
    this.productSearchRequest.pageSize = pageSize || 1;
  }

  get pageSize(): number {
    return this.productSearchRequest.pageSize;
  }

  getFacets(): Facet[] {
    return this.productSearchResult?.facets || [];
  }

  next(): void {
    if (this.hasNextPage()) {
      this.productSearchRequest.startIndex = this.productSearchRequest.startIndex + this.productSearchRequest.pageSize;
    }
  }

  previous(): void {
    if (this.hasPreviousPage()) {
      this.productSearchRequest.startIndex = this.productSearchRequest.startIndex - this.productSearchRequest.pageSize;
    }
  }

  facetSelectionChanged(facetId: string, value: string | number, isSelected: boolean): void {
    this.productSearchResult = _.cloneDeep(this.productSearchResult);
    const facetToMarkSelected = this.productSearchResult?.facets.find((facet: Facet) => facet.field === facetId);
    const selectedFacetValue = facetToMarkSelected?.values.find((facetValue) => facetValue.value === value);
    if (selectedFacetValue) selectedFacetValue.isApplied = isSelected;

    this.productSearchResult?.facets.forEach((facet) => {
      facet.isFaceted = facet.values.filter((fval) => fval.isApplied).length > 0;
    });

    const appliedFacets = this.productSearchResult?.facets.filter((facet: Facet) => facet.isFaceted) || [];
    const facetValueFilters: string[] = [];
    appliedFacets.forEach((appliedFacet) => {
      appliedFacet.values
        .filter((facetValue) => facetValue.isApplied)
        .forEach((appliedValue) => facetValueFilters.push(appliedValue.filterValue));
    });
    this.productSearchRequest.facetValueFilter = facetValueFilters.join(',');
    this.productSearchRequest.startIndex = 0; // TODO - Keep page size only if selected facet has those many products
  }

  public setHierarchy(hierarchyField: string, hierarchyValue: string | number | boolean): void {
    this.productSearchRequest.filter = hierarchyField + ' req ' + hierarchyValue;
    this.productSearchRequest.facetTemplate = hierarchyField + ':' + hierarchyValue;
    this.productSearchRequest.facetHierValue = hierarchyField + ':' + hierarchyValue;
  }
}

export default SearchApi;
