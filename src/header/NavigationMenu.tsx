import * as React from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Link from '@material-ui/core/Link';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { NavigationNode } from '../@types/Navigation';

export type NavigationMenuProps = {
  navItem: NavigationNode;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    navBarItem: {
      color: theme.palette.primary.contrastText,
      paddingLeft: theme.spacing(1.5),
      paddingRight: theme.spacing(1.5),
      fontSize: '1rem',
      textDecoration: 'none',
      cursor: 'pointer',
      display: 'flex',
      alignItems: 'center',
      '&:hover': {
        textDecoration: 'none',
        backgroundColor: theme.palette.primary.dark,
      },
      '&[aria-controls="menu-list-grow"]': {
        backgroundColor: theme.palette.primary.dark,
        '&:after': {
          content: "' '",
          position: 'absolute',
          borderColor: 'rgba(194, 225, 245, 0)',
          border: 'solid transparent',
          borderBottomColor: 'white',
          borderWidth: '11px',
          top: '65%',
          right: '45%',
          zIndex: '1',
        },
      },
    },
    menuItem: {
      '&:hover': {
        backgroundColor: theme.palette.primary.main,
      },
      '&:hover > a': {
        textDecoration: 'none',
        color: theme.palette.primary.contrastText,
      },
    },
    menuItemLink: {
      flex: 1,
    },
  }),
);

export default function NavigationMenu(props: NavigationMenuProps): JSX.Element {
  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current && !open) {
      anchorRef.current?.focus();
    }

    prevOpen.current = open;
  }, [open]);

  if (props.navItem && props.navItem.items && props.navItem.items.length > 0) {
    return (
      <>
        <Link
          component="button"
          ref={anchorRef}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
          className={classes.navBarItem}
        >
          {props.navItem.name}
        </Link>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
          style={{ zIndex: 500 }}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                    {props.navItem.items?.map((navItem, index) => (
                      <MenuItem
                        onClick={(e) => {
                          const navUrl = navItem.nodeType === 'category' ? `/c/${navItem.categoryId}` : navItem.url;
                          history.push(navUrl);
                          handleClose(e);
                        }}
                        key={index}
                        className={classes.menuItem}
                      >
                        <Link
                          component={RouterLink}
                          to={navItem.nodeType === 'category' ? `/c/${navItem.categoryId}` : navItem.url}
                          className={classes.menuItemLink}
                        >
                          {navItem.name}
                        </Link>
                      </MenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </>
    );
  } else if (props.navItem && props.navItem.name && props.navItem.url) {
    const navUrl = props.navItem.nodeType === 'category' ? `/c/${props.navItem.categoryId}` : props.navItem.url;
    return (
      <Link component={RouterLink} to={navUrl} className={classes.navBarItem}>
        {props.navItem.name}
      </Link>
    );
  }

  return <></>;
}
