import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import clsx from 'clsx';
import NavigationBar from './NavigationBar';
import Api from '../api/Api';
import PageContext from '../@types/pageContext/PageContext';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    flexGrow: {
      flexGrow: 1,
    },
    siteHeadingRow: {
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
    },
  }),
);

export default function Header(): JSX.Element {
  const classes = useStyles();
  const api = new Api();

  // Just like page-context and api-context, site-context will always be available.
  const pageContext = api.getPreloadData<PageContext>('pagecontext') as PageContext;

  return (
    <header className={classes.flexGrow}>
      <Typography
        variant="h4"
        component="h1"
        className={clsx(classes.flexGrow, classes.siteHeadingRow)}
        role="heading"
        aria-level={1}
      >
        {pageContext.title}
      </Typography>
      <NavigationBar />
    </header>
  );
}
