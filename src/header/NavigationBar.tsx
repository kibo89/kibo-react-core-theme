import * as React from 'react';
import StoreWindow from '../@types/StoreWindow';
import NavigationMenu from './NavigationMenu';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Navigation from '../@types/Navigation';

declare let window: StoreWindow;

const useStyles = makeStyles(() =>
  createStyles({
    toolbar: {
      alignItems: 'stretch',
    },
  }),
);

export default function NavigationBar(): JSX.Element {
  const classes = useStyles();
  const navigation: Navigation = window.navigation;
  const navItemsJSX = navigation.tree.map((navItem, navIndex) => {
    if ((navItem && navItem.items && navItem.items.length > 0) || (navItem && navItem.name && navItem.url)) {
      return <NavigationMenu navItem={navItem} key={navIndex} />;
    }

    return null;
  });

  return (
    <AppBar position="static" component="div">
      <Toolbar component="nav" role="navigation" aria-label="Categories and More" className={classes.toolbar}>
        {navItemsJSX}
      </Toolbar>
    </AppBar>
  );
}
