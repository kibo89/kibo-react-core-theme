import * as React from 'react';
import Dropzone from '../kibo/Dropzone';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type HomeProps = any;

export default class Home extends React.Component<HomeProps> {
  render(): JSX.Element {
    return (
      <div>
        Home
        <Dropzone zoneId="homeBanner" hintText="Drop here home banner" />
        <Dropzone zoneId="homeBanner1" />
      </div>
    );
  }
}
