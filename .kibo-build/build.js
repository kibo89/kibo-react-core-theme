const commander = require('commander');
const glob = require('glob');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const application = require('mozu-node-sdk/clients/platform/application');
const fiddlerProxy = require('mozu-node-sdk/plugins/fiddler-proxy');

const DEV = 'DEVELOPER';
const PATHSEP = '|';
const CURRENT = './';
const CONCURRENT_REQUESTS = 16;

class Build {
  constructor() {
    this.program = new commander.Command();

    this.program.option('--upload', 'Upload code to Kibo theme')
      .option('--wipe', 'Wipe code from Kibo theme')
      .option('--ignoreChecksum', 'Ignore checksum while uploading file')
      .requiredOption('-k, --appKey <appKey>', 'Application key of the theme')
      .requiredOption('-u, --userName <userName>', 'Kibo account user-name')
      .requiredOption('-p, --password <password>', 'Kibo account password')
      .requiredOption('-i, --devAccountId <devAccountId>', 'Kibo dev. account ID');

    this.program.parse(process.argv)

    const context = {
      baseUrl: 'https://home.mozu.com',
      developerAccountId: this.program.devAccountId,
      appKey: this.program.appKey,
      developerAccount: {
        emailAddress: this.program.userName,
        password: this.program.password,
      },
    };

    if (!this.program.upload && !this.program.wipe) throw new Error('Either of upload or wipe option is required.');

    const startTime = Date.now();
    this.init(context).then(() => {
      const endTime = Date.now();
      const operationTime = parseInt(`${(endTime - startTime) / 1000}`);
      console.log([...Array(10).keys()].map(() => '=').join(''));
      const timeStr = `${operationTime > 0 ? `${operationTime} sec.` : `${(endTime - startTime)} ms.`}`;
      console.log(`Operation(s) finished in ${timeStr}`);
    }).catch((e) => {
      console.error(e);
      process.exit(1);
    });
  }

  async init(context) {
    this.kiboClient = this.createClient(context, {});

    const existingFilesList = [];
    try {
      const metaData = await this.kiboClient.getPackageMetadata({
        applicationKey: context.appKey,
        // filepath: './assets/functions.json'
      }, { scope: DEV });

      if (metaData) {
        existingFilesList.push(...this.metadataTreeToArray([metaData]));
      }
    } catch (error) {
      console.error(error.message);
      // No need to deal with this error.
    }

    if (!this.kiboClient.context['user-claims']) {
      console.error('Failed to authenticate');
      throw new Error('Failed to authenticate');
    }

    if (this.program.wipe) {
      console.group('Deleting Files');

      await this.logTime('Deletion', async () => {
        let entriesProcessed = 0;

        // Delete files.
        if (existingFilesList && existingFilesList.length) {
          const chunks = (existingFilesList.length / CONCURRENT_REQUESTS)
            + ((existingFilesList.length % CONCURRENT_REQUESTS) > 0 ? 1 : 0)

          for (let i = 0; i < chunks; i++) {
            const startIndex = i * CONCURRENT_REQUESTS;
            const currentDeletionList = existingFilesList.slice(startIndex, startIndex + CONCURRENT_REQUESTS);
            entriesProcessed += (await this.deleteFiles(context, currentDeletionList)).length;
          }
        } else {
          console.log('Nothing is available for deletion.');
        }

        console.groupEnd();
        console.log(`Deleted ${entriesProcessed} of ${existingFilesList.length} files`);
      });
    }

    if (this.program.upload) {
      if (this.program.wipe) {
        this.program.wipe = false;
        await this.init(context);

        return;
      }

      // Upload files.
      const uploadList = await this.getUploadFilesList();
      if (uploadList && uploadList.length) {
        console.group('Uploading Files');
        await this.logTime('Upload', async () => {
          let entriesProcessed = 0;
          const chunks = (uploadList.length / CONCURRENT_REQUESTS)
            + ((uploadList.length % CONCURRENT_REQUESTS) > 0 ? 1 : 0)

          for (let i = 0; i < chunks; i++) {
            const startIndex = i * CONCURRENT_REQUESTS;
            const currentUploadList = uploadList.slice(startIndex, startIndex + CONCURRENT_REQUESTS);
            entriesProcessed += (await this.uploadFiles(context, currentUploadList, existingFilesList)).length;
          }

          console.groupEnd();
          console.log(`Uploaded ${entriesProcessed} of ${uploadList.length} files`);
        });
      } else {
        console.log('Nothing is available for uploading.');
      }
    }
  }

  async logTime(operationName, operation) {
    const startTime = Date.now();

    let returnVal = null;
    if (operation && typeof operation === 'function') {
      try {
        returnVal = await operation();
      } catch (err) {
        returnVal = err;
      }
    }

    const endTime = Date.now();
    const operationTime = parseInt(`${(endTime - startTime) / 1000}`);
    const timeStr = `${operationTime > 0 ? `${operationTime} sec.` : `${(endTime - startTime)} ms.`}`;
    console.log(`${operationName ? operationName : 'operation'} finished in ${timeStr}`);

    return {
      operationResult: returnVal,
      operationTime: timeStr,
    };
  }

  uploadFiles(context, uploadList, existingFilesList) {
    const uploadPromiseList = [];

    for (let i = 0; i < uploadList.length; i++) {
      const uploadEntry = uploadList[i];
      if (!uploadEntry.filePath) continue;

      const fsStats = fs.lstatSync(uploadEntry.filePath);

      // const fileContents = fs.readFileSync(uploadEntry.filePath);
      const fileContents = this.getTransformedFileContents(uploadEntry.filePath);

      const existingFileEntry = existingFilesList.find(
        (entry) => entry.path === this.normalizedPath(uploadEntry.uploadPath)
      );
      const fileChecksum = this.getChecksum(fileContents);
      const checksumMatches = existingFileEntry && existingFileEntry.checkSum === fileChecksum;

      if (!checksumMatches || this.program.ignoreChecksum) {
        console.log(`[${this.getReadableFileSizeString(fsStats.size)}] ${this.normalizedPath(uploadEntry.uploadPath)}`);

        uploadPromiseList.push(this.uploadFile({
          applicationKey: context.appKey,
          filepath: uploadEntry.uploadPath,
          fileContents: fileContents,
        }));
      } else {
        // console.log(`[SKIPPED] ${uploadEntry.filePath}`);
      }
    }

    return Promise.allSettled(uploadPromiseList);
  }

  deleteFiles(context, existingFilesList) {
    const deletionPromiseList = [];

    for (let i = 0; i < existingFilesList.length; i++) {
      console.log(`[DELETED] ${existingFilesList[i].path}`);

      deletionPromiseList.push(this.deleteFile({
        applicationKey: context.appKey,
        filepath: this.formatPath(existingFilesList[i].path),
      }));
    }

    return Promise.allSettled(deletionPromiseList);
  }

  uploadFile(config) {
    return this.kiboClient.upsertPackageFile({
      applicationKey: config.applicationKey,
      filepath: config.filepath,
    }, {
      scope: DEV,
      body: config.fileContents,
    });
  }

  deleteFile(config) {
    return this.kiboClient.deletePackageFile({
      applicationKey: config.applicationKey,
      filepath: config.filepath,
    }, {
      scope: DEV
    });
  }

  metadataTreeToArray(trees) {
    return trees.reduce((files, tree) => {
      return files.concat(tree.files).concat(tree.subFolders.length === 0 ? [] : this.metadataTreeToArray(tree.subFolders));
    }, []);
  }

  async getUploadFilesList() {
    const uploadList = [];
    uploadList.push(...await this.getUploadPaths(
      'static/js/**', 'build', 'scripts'));

    /**
     * CSS is supposed to be in stylesheets folder but kibo moves it in cdn
     * which is perfectly fine but then browser throws an error for CORS when we try to refer the resource file
     * (ex woff or woff2 font file) in CSS from resources directory.
     * So moving it to resources folder is the working alternate approach.
     */
    uploadList.push(...await this.getUploadPaths(
      'static/css/**', 'build', 'resources'));

    uploadList.push(...await this.getUploadPaths(
      'static/media/**', 'build', 'resources'));
    uploadList.push(...await this.getUploadPaths(
      'assets/images/**', 'build', 'resources'));
    uploadList.push(...await this.getUploadPaths('**', 'kibo-core'));

    uploadList.push({
      filePath: 'build/index.html',
      uploadPath: 'templates|page.hypr'
    });

    uploadList.push(...await this.getUploadPaths('*', 'build', 'resources'));
    uploadList.push(...await this.getUploadPaths('*', 'kibo-core', 'resources'));

    return uploadList;
  }

  getReadableFileSizeString(fileSizeInBytes) {
    let i = -1;
    const byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
    do {
      fileSizeInBytes = fileSizeInBytes / 1024;
      i++;
    } while (fileSizeInBytes > 1024);

    const sizeStr = Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];

    const totalStrSize = 12;
    return (Array(parseInt('' + (totalStrSize - sizeStr.length) / 2) + 1)
      .join(' ') + sizeStr).padEnd(totalStrSize, ' ');
  }

  getUploadPaths(directoryToScan, cwd, uploadPathPrefix) {
    return new Promise((resolve) => {
      glob(directoryToScan, {
        cwd: cwd
      }, (err, filesFound) => {
        if (err) {
          resolve([]);

          return;
        }

        const filteredFilesList = filesFound.filter((filePath) => {
          const fsStats = fs.lstatSync(cwd + path.sep + filePath);

          return !fsStats.isDirectory();
        }).map((fileEntry) => {
          return {
            filePath: cwd + path.sep + fileEntry,
            uploadPath: this.formatPath((uploadPathPrefix ? uploadPathPrefix + path.sep : '') + fileEntry)
          };
        });

        resolve(filteredFilesList);
      });
    });
  }

  formatPath(pathstring) {
    return path.join(CURRENT, pathstring).split(path.sep).join(PATHSEP);
  }

  normalizedPath(pathstring) {
    return path.join(CURRENT, pathstring).split(PATHSEP).join(path.sep);
  }

  getChecksum(fileText) {
    const hash = crypto.createHash('md5');
    hash.update(fileText);
    return hash.digest('hex');
  }

  /**
   * TODO This function is just to make things work in kibo context but needs to be revisited
   * @param actualFilePath File path on local system
   * @returns {Buffer} File contents buffer (if required then modified in case of HTML & CSS)
   */
  getTransformedFileContents(actualFilePath) {
    let fileContents = fs.readFileSync(actualFilePath);

    if (actualFilePath.startsWith('build/static/css/')) {
      fileContents = fileContents.toString();
      // Process css file.
      fileContents = fileContents.split('url(/scripts/static/media').join('url(/resources/static/media');
      fileContents = fileContents.split('url("/scripts/static/media').join('url("/resources/static/media');
      fileContents = fileContents.split('url(\'/scripts/static/media').join('url(\'/resources/static/media');

      fileContents = Buffer.from(fileContents);
    } else if (actualFilePath === 'build/index.html') {
      fileContents = fileContents.toString();
      // process html file.
      fileContents = fileContents.split('<link href="/scripts/static/css')
        .join('<link href="/resources/static/css');
      fileContents = fileContents.split('<link href="/scripts/static/media')
        .join('<link href="/resources/static/media');

      // dynamic css module loading
      const contentsArr = fileContents.split('".chunk.css",');
      const contextPrefixExpression = contentsArr[1].substring(0, contentsArr[1].indexOf(','));
      fileContents = contentsArr[0]
        + '".chunk.css",'
        + contextPrefixExpression.split('=')[0] + '="/resources/"+'
        + contextPrefixExpression.split('=')[1].split('+')[1] + ','
        + contentsArr[1].substring(contentsArr[1].indexOf(',') + 1, contentsArr[1].length);

      fileContents = Buffer.from(fileContents);
    }

    return fileContents;
  }

  createClient(context, config) {
    config = config || {};
    config.context = context;
    config.plugins = config.plugins || [];
    config.plugins.push(fiddlerProxy);
    config.defaultRequestOptions = {
      timeout: 60000
    };

    return application(config);
  }
}

module.exports = new Build();
